@IsTest
public with sharing class OMStdAndPlusDecompositionCompareTest
{
	@IsTest
	public static void testDecomposition_8013h0000013JczAAE()
	{
		XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();
		XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);

		fixture.prod('XOMComm1').scope('Order Item').globalKey('b4c42b16-e34e-e742-4a45-9d40ea3e1f6c').end();
		fixture.prod('XOMComm1.2').scope('Order Item').globalKey('10b1600a-17e8-a1f8-abb7-9e903906f688').end();
		fixture.pci('XOMComm1', 'XOMComm1.2').end();
		fixture.prod('XOMComm1.1').scope('Order Item').globalKey('1ec1176a-d6e8-da6e-37d4-9c2351aae136').end();
		fixture.pci('XOMComm1', 'XOMComm1.1').end();
		fixture.prod('XOMTech1').scope('Account').globalKey('dde096b3-340f-c6be-5f29-98d3f8a5deb4').end();
		fixture.dr('XOMComm1.1', 'XOMTech1')
		.end();
		fixture.prod('XOMTech1.1').scope('Order Item').globalKey('ceb09d44-260d-9c57-98f5-65c113392149').end();
		fixture.dr('XOMTech1', 'XOMTech1.1')
		.end();
		fixture.prod('XOMTech1.2').scope('Order Item').globalKey('be7433eb-6b4e-442b-c105-1d8d86b193b9').end();
		fixture.dr('XOMComm1.2', 'XOMTech1.2')
		.end();
		fixture.dr('XOMComm1.2', 'XOMTech1')
		.end();
		fixture.pci('XOMTech1', 'XOMTech1.2').end();
		fixture.pci('XOMTech1', 'XOMTech1.1').end();

		fixture
		.account('Test')
		.order(false)
		.oi('0001', 'XOMComm1').end()
		.oi('0001.0001', 'XOMComm1.1').end()
		.oi('0001.0002', 'XOMComm1.2').end()
		.end()
		.end();

		fixture.persist();
		XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();
		List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;

		System.assertEquals(2, frList.size());
		List<FulfilmentRequestLineDecompRelationship__c> frlDecompRelationshipsList = [SELECT  Id, SourceOrderItemId__c, SourceOrderItemId__r.Product2Id__c,SourceOrderItemId__r.Product2Id__r.GlobalKey__c, SourceFulfilmentRequestLineId__c,SourceFulfilmentRequestLineId__r.Product2Id__c,SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c , DestinationFulfilmentRequestLineId__c,DestinationFulfilmentRequestLineId__r.Product2Id__c,DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c from FulfilmentRequestLineDecompRelationship__c where DestinationFulfilmentRequestLineId__r.FulfilmentRequestID__r.OrderId__c = '8013h0000013JczAAE'];
		Map<String, Map<String, Integer>> frlDecompRelationshipsMap = DecompositionCompareTestHelper.testDecomposition_8013h0000013JczAAE();

		for(FulfilmentRequestLineDecompRelationship__c frlDecomRelationship : frlDecompRelationshipsList)
		{
			String dstGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
			System.assert(frlDecompRelationshipsMap.containsKey(dstGlobalKey));

			Map<String, Integer> srcMap = frlDecompRelationshipsMap.get(dstGlobalKey);
			String srcGlobalKey = null;

			if(frlDecomRelationship.SourceFulfilmentRequestLineId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
				}
			else if(frlDecomRelationship.SourceOrderItemId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceOrderItemId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
			}
			Integer count = srcMap.get(srcGlobalKey);
			count--;

			if(count ==0)
			{
				srcMap.remove(srcGlobalKey);

				if(srcMap.isEmpty())
				{
					frlDecompRelationshipsMap.remove(dstGlobalKey);
				}
				else
				{
					srcMap.put(srcGlobalKey, count);
				}
			}
		}
	}

	@IsTest
	public static void testDecomposition_8013h0000013Hb2AAE()
	{
		XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();
		XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);

		fixture.prod('XOMComm1').scope('Order Item').globalKey('b4c42b16-e34e-e742-4a45-9d40ea3e1f6c').end();
		fixture.prod('XOMComm1.2').scope('Order Item').globalKey('10b1600a-17e8-a1f8-abb7-9e903906f688').end();
		fixture.pci('XOMComm1', 'XOMComm1.2').end();
		fixture.prod('XOMComm1.1').scope('Order Item').globalKey('1ec1176a-d6e8-da6e-37d4-9c2351aae136').end();
		fixture.pci('XOMComm1', 'XOMComm1.1').end();
		fixture.prod('XOMTech1').scope('Account').globalKey('dde096b3-340f-c6be-5f29-98d3f8a5deb4').end();
		fixture.dr('XOMComm1.1', 'XOMTech1')
		.end();
		fixture.prod('XOMTech1.1').scope('Order Item').globalKey('ceb09d44-260d-9c57-98f5-65c113392149').end();
		fixture.dr('XOMTech1', 'XOMTech1.1')
		.end();

		fixture
		.account('Test')
		.order(false)
		.oi('0001', 'XOMComm1').end()
		.oi('0001.0001', 'XOMComm1.1').end()
		.end()
		.end();

		fixture.persist();
		XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();
		List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;

		System.assertEquals(1, frList.size());
		List<FulfilmentRequestLineDecompRelationship__c> frlDecompRelationshipsList = [SELECT  Id, SourceOrderItemId__c, SourceOrderItemId__r.Product2Id__c,SourceOrderItemId__r.Product2Id__r.GlobalKey__c, SourceFulfilmentRequestLineId__c,SourceFulfilmentRequestLineId__r.Product2Id__c,SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c , DestinationFulfilmentRequestLineId__c,DestinationFulfilmentRequestLineId__r.Product2Id__c,DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c from FulfilmentRequestLineDecompRelationship__c where DestinationFulfilmentRequestLineId__r.FulfilmentRequestID__r.OrderId__c = '8013h0000013Hb2AAE'];
		Map<String, Map<String, Integer>> frlDecompRelationshipsMap = DecompositionCompareTestHelper.testDecomposition_8013h0000013Hb2AAE();

		for(FulfilmentRequestLineDecompRelationship__c frlDecomRelationship : frlDecompRelationshipsList)
		{
			String dstGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
			System.assert(frlDecompRelationshipsMap.containsKey(dstGlobalKey));

			Map<String, Integer> srcMap = frlDecompRelationshipsMap.get(dstGlobalKey);
			String srcGlobalKey = null;

			if(frlDecomRelationship.SourceFulfilmentRequestLineId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
				}
			else if(frlDecomRelationship.SourceOrderItemId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceOrderItemId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
			}
			Integer count = srcMap.get(srcGlobalKey);
			count--;

			if(count ==0)
			{
				srcMap.remove(srcGlobalKey);

				if(srcMap.isEmpty())
				{
					frlDecompRelationshipsMap.remove(dstGlobalKey);
				}
				else
				{
					srcMap.put(srcGlobalKey, count);
				}
			}
		}
	}

	@IsTest
	public static void testDecomposition_8013h0000011j3IAAQ()
	{
		XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();
		XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);

		fixture.prod('XomSrc').scope('Order Item').globalKey('eab204c7-834a-0dcc-1e39-c78a5716cdb7').end();
		fixture.prod('XOMSrc1').scope('Order Item').globalKey('d615f4da-1d30-07c0-e07b-e4ed6838a2d7').end();
		fixture.pci('XomSrc', 'XOMSrc1').end();
		fixture.prod('XOMSrc2').scope('Order Item').globalKey('c8292463-f32b-74c5-65a1-41d7046b0f2d').end();
		fixture.pci('XOMSrc1', 'XOMSrc2').end();
		fixture.prod('XOMDst1').scope('Order Item').globalKey('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f').end();
		fixture.dr('XOMSrc1', 'XOMDst1')
		.end();
		fixture.prod('XOMDst2').scope('Downstream Order Item').globalKey('1f045892-de43-a24f-d349-4cf7f2d71834').end();
		fixture.dr('XOMSrc2', 'XOMDst2')
		.end();
		fixture.pci('XOMDst1', 'XOMDst2').end();
		fixture.prod('XOMDst3').scope('Order Item').globalKey('761caa30-e98b-f3a9-48c4-33cf41061240').end();
		fixture.dr('XOMDst2', 'XOMDst3')
		.end();
		fixture.pci('XOMDst1', 'XOMDst3').end();

		fixture
		.account('Test')
		.order(false)
		.oi('0001', 'XomSrc').end()
		.oi('0001.0001', 'XOMSrc1').end()
		.oi('0001.0001.0001', 'XOMSrc2').end()
		.oi('0002', 'XomSrc').end()
		.oi('0002.0001', 'XOMSrc1').end()
		.oi('0002.0001.0001', 'XOMSrc2').end()
		.end()
		.end();

		fixture.persist();
		XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();
		List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;

		System.assertEquals(4, frList.size());
		List<FulfilmentRequestLineDecompRelationship__c> frlDecompRelationshipsList = [SELECT  Id, SourceOrderItemId__c, SourceOrderItemId__r.Product2Id__c,SourceOrderItemId__r.Product2Id__r.GlobalKey__c, SourceFulfilmentRequestLineId__c,SourceFulfilmentRequestLineId__r.Product2Id__c,SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c , DestinationFulfilmentRequestLineId__c,DestinationFulfilmentRequestLineId__r.Product2Id__c,DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c from FulfilmentRequestLineDecompRelationship__c where DestinationFulfilmentRequestLineId__r.FulfilmentRequestID__r.OrderId__c = '8013h0000011j3IAAQ'];
		Map<String, Map<String, Integer>> frlDecompRelationshipsMap = DecompositionCompareTestHelper.testDecomposition_8013h0000011j3IAAQ();

		for(FulfilmentRequestLineDecompRelationship__c frlDecomRelationship : frlDecompRelationshipsList)
		{
			String dstGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
			System.assert(frlDecompRelationshipsMap.containsKey(dstGlobalKey));

			Map<String, Integer> srcMap = frlDecompRelationshipsMap.get(dstGlobalKey);
			String srcGlobalKey = null;

			if(frlDecomRelationship.SourceFulfilmentRequestLineId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
				}
			else if(frlDecomRelationship.SourceOrderItemId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceOrderItemId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
			}
			Integer count = srcMap.get(srcGlobalKey);
			count--;

			if(count ==0)
			{
				srcMap.remove(srcGlobalKey);

				if(srcMap.isEmpty())
				{
					frlDecompRelationshipsMap.remove(dstGlobalKey);
				}
				else
				{
					srcMap.put(srcGlobalKey, count);
				}
			}
		}
	}

	@IsTest
	public static void testDecomposition_8013h0000011lDHAAY()
	{
		XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();
		XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);

		fixture.prod('A_Product_F').scope('Order Item').globalKey('cc51d34d-ef53-ae7b-a0fa-74549258903f').end();
		fixture.prod('A_Product_F1').scope('Order Item').globalKey('0b6e40c5-739a-cfe1-114a-f523988679e2').end();
		fixture.pci('A_Product_F', 'A_Product_F1').end();
		fixture.prod('A_Product_F_FRL').scope('Order Item').globalKey('e73de1bf-b97f-d4f1-3f7b-2b605d3057c5').end();
		fixture.dr('A_Product_F', 'A_Product_F_FRL')
		.end();
		fixture.prod('A_Product_F1_FRL').scope('Order Item').globalKey('f901b323-ff2e-7e6e-8cb8-b9df38f840c2').end();
		fixture.dr('A_Product_F1', 'A_Product_F1_FRL')
		.end();
		fixture.pci('A_Product_F_FRL', 'A_Product_F1_FRL').end();

		fixture
		.account('Test')
		.order(false)
		.oi('0001', 'A_Product_F').end()
		.oi('0001.0001', 'A_Product_F1').end()
		.end()
		.end();

		fixture.persist();
		XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();
		List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;

	}

	@IsTest
	public static void testDecomposition_8013h0000011j3DAAQ()
	{
		XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();
		XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);

		fixture.prod('XomSrc').scope('Order Item').globalKey('eab204c7-834a-0dcc-1e39-c78a5716cdb7').end();
		fixture.prod('XOMSrc1').scope('Order Item').globalKey('d615f4da-1d30-07c0-e07b-e4ed6838a2d7').end();
		fixture.pci('XomSrc', 'XOMSrc1').end();
		fixture.prod('XOMSrc2').scope('Order Item').globalKey('c8292463-f32b-74c5-65a1-41d7046b0f2d').end();
		fixture.pci('XOMSrc1', 'XOMSrc2').end();
		fixture.prod('XOMDst1').scope('Order Item').globalKey('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f').end();
		fixture.dr('XOMSrc1', 'XOMDst1')
		.end();
		fixture.prod('XOMDst2').scope('Downstream Order Item').globalKey('1f045892-de43-a24f-d349-4cf7f2d71834').end();
		fixture.dr('XOMSrc2', 'XOMDst2')
		.end();
		fixture.pci('XOMDst1', 'XOMDst2').end();
		fixture.prod('XOMDst3').scope('Order Item').globalKey('761caa30-e98b-f3a9-48c4-33cf41061240').end();
		fixture.dr('XOMDst2', 'XOMDst3')
		.end();

		fixture
		.account('Test')
		.order(false)
		.oi('0001', 'XomSrc').end()
		.oi('0001.0001', 'XOMSrc1').end()
		.oi('0001.0001.0001', 'XOMSrc2').end()
		.end()
		.end();

		fixture.persist();
		XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();
		List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;

		System.assertEquals(2, frList.size());
		List<FulfilmentRequestLineDecompRelationship__c> frlDecompRelationshipsList = [SELECT  Id, SourceOrderItemId__c, SourceOrderItemId__r.Product2Id__c,SourceOrderItemId__r.Product2Id__r.GlobalKey__c, SourceFulfilmentRequestLineId__c,SourceFulfilmentRequestLineId__r.Product2Id__c,SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c , DestinationFulfilmentRequestLineId__c,DestinationFulfilmentRequestLineId__r.Product2Id__c,DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c from FulfilmentRequestLineDecompRelationship__c where DestinationFulfilmentRequestLineId__r.FulfilmentRequestID__r.OrderId__c = '8013h0000011j3DAAQ'];
		Map<String, Map<String, Integer>> frlDecompRelationshipsMap = DecompositionCompareTestHelper.testDecomposition_8013h0000011j3DAAQ();

		for(FulfilmentRequestLineDecompRelationship__c frlDecomRelationship : frlDecompRelationshipsList)
		{
			String dstGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
			System.assert(frlDecompRelationshipsMap.containsKey(dstGlobalKey));

			Map<String, Integer> srcMap = frlDecompRelationshipsMap.get(dstGlobalKey);
			String srcGlobalKey = null;

			if(frlDecomRelationship.SourceFulfilmentRequestLineId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
				}
			else if(frlDecomRelationship.SourceOrderItemId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceOrderItemId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
			}
			Integer count = srcMap.get(srcGlobalKey);
			count--;

			if(count ==0)
			{
				srcMap.remove(srcGlobalKey);

				if(srcMap.isEmpty())
				{
					frlDecompRelationshipsMap.remove(dstGlobalKey);
				}
				else
				{
					srcMap.put(srcGlobalKey, count);
				}
			}
		}
	}

	@IsTest
	public static void testDecomposition_8013h0000011j3NAAQ()
	{
		XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();
		XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);

		fixture.prod('Unlimited Plan').scope('Order Item').globalKey('d2fc20a5-ddac-3f65-577a-35060d333959').end();
		fixture.prod('RFS MTAS Activation').scope('Account').globalKey('413319ee-531a-cc0a-abe6-158550c49018').end();
		fixture.dr('Unlimited Plan', 'RFS MTAS Activation')
		.end();
		fixture.prod('RFS MTAS VM Preactivation').scope('Account').globalKey('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801').end();
		fixture.dr('Unlimited Plan', 'RFS MTAS VM Preactivation')
		.end();
		fixture.prod('iPhone').scope('Order Item').globalKey('8e88078e-d784-ee57-ec9a-87dbeb1a0de2').end();
		fixture.dr('iPhone', 'RFS MTAS Activation')
		.end();
		fixture.dr('iPhone', 'RFS MTAS VM Preactivation')
		.end();
		fixture.prod('Visible SIM').scope('Order Item').globalKey('6b9b1b28-0690-cc72-f731-e7f13880fa35').end();
		fixture.prod('Decomp EPS MDN Activation').scope('Order Item').globalKey('859d58f9-2274-d717-b661-49d58cd6390e').end();
		fixture.dr('Visible SIM', 'Decomp EPS MDN Activation')
		.end();
		fixture.dr('Visible SIM', 'RFS MTAS VM Preactivation')
		.end();
		fixture.dr('Visible SIM', 'RFS MTAS Activation')
		.end();
		fixture.prod('Decomp Reseller MDN Activation').scope('Order Item').globalKey('c94b8510-c464-e9db-8a1f-ad615afea11b').end();
		fixture.dr('Visible SIM', 'Decomp Reseller MDN Activation')
		.end();
		fixture.prod('Decomp Vision MDN Activation').scope('Order Item').globalKey('a04eddd7-3f37-ffaf-2046-e6108b5861c5').end();
		fixture.dr('Visible SIM', 'Decomp Vision MDN Activation')
		.end();
		fixture.prod('Decomp PI MDN Activation').scope('Order Item').globalKey('beec356f-881a-78c4-f83b-610fe75db48f').end();
		fixture.dr('Visible SIM', 'Decomp PI MDN Activation')
		.end();
		fixture.prod('Decomp New MDN Activation').scope('Order Item').globalKey('96df3947-cec7-3bb2-61ec-dcbaadc02b34').end();
		fixture.dr('Visible SIM', 'Decomp New MDN Activation')
		.end();
		fixture.prod('SIM Fulfilment Scope').scope('Order Item').globalKey('2f4d5c34-1eb6-e59e-914f-6c06d6a41d6a').end();
		fixture.dr('Visible SIM', 'SIM Fulfilment Scope')
		.end();

		fixture
		.account('Test')
		.order(false)
		.oi('0003', 'Unlimited Plan').end()
		.oi('0001', 'iPhone').end()
		.oi('0002', 'Visible SIM').end()
		.end()
		.end();

		fixture.persist();
		XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();
		List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;

		System.assertEquals(8, frList.size());
		List<FulfilmentRequestLineDecompRelationship__c> frlDecompRelationshipsList = [SELECT  Id, SourceOrderItemId__c, SourceOrderItemId__r.Product2Id__c,SourceOrderItemId__r.Product2Id__r.GlobalKey__c, SourceFulfilmentRequestLineId__c,SourceFulfilmentRequestLineId__r.Product2Id__c,SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c , DestinationFulfilmentRequestLineId__c,DestinationFulfilmentRequestLineId__r.Product2Id__c,DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c from FulfilmentRequestLineDecompRelationship__c where DestinationFulfilmentRequestLineId__r.FulfilmentRequestID__r.OrderId__c = '8013h0000011j3NAAQ'];
		Map<String, Map<String, Integer>> frlDecompRelationshipsMap = DecompositionCompareTestHelper.testDecomposition_8013h0000011j3NAAQ();

		for(FulfilmentRequestLineDecompRelationship__c frlDecomRelationship : frlDecompRelationshipsList)
		{
			String dstGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
			System.assert(frlDecompRelationshipsMap.containsKey(dstGlobalKey));

			Map<String, Integer> srcMap = frlDecompRelationshipsMap.get(dstGlobalKey);
			String srcGlobalKey = null;

			if(frlDecomRelationship.SourceFulfilmentRequestLineId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
				}
			else if(frlDecomRelationship.SourceOrderItemId__r != null)
			{
				srcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceOrderItemId__r.Product2Id__r.GlobalKey__c);
				System.assert(srcMap.containsKey(srcGlobalKey));
			}
			Integer count = srcMap.get(srcGlobalKey);
			count--;

			if(count ==0)
			{
				srcMap.remove(srcGlobalKey);

				if(srcMap.isEmpty())
				{
					frlDecompRelationshipsMap.remove(dstGlobalKey);
				}
				else
				{
					srcMap.put(srcGlobalKey, count);
				}
			}
		}
	}

}

