public with sharing class DecompositionCompareTestHelper
{
	public static Map<String, Map<String, Integer>> testDecomposition_8013h0000013JczAAE()
	{
		Map<String, Map<String, Integer>> destinationDRGlobalKeyMap = new Map<String, Map<String, Integer>>();
		Map<String, Integer> sourceDRGlobalKeyMap;
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('dde096b3-340f-c6be-5f29-98d3f8a5deb4'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('dde096b3-340f-c6be-5f29-98d3f8a5deb4');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('1ec1176a-d6e8-da6e-37d4-9c2351aae136'))
			{
				count = sourceDRGlobalKeyMap.get('1ec1176a-d6e8-da6e-37d4-9c2351aae136');
			}
			count++;
			sourceDRGlobalKeyMap.put('1ec1176a-d6e8-da6e-37d4-9c2351aae136', count);
		}
		destinationDRGlobalKeyMap.put('dde096b3-340f-c6be-5f29-98d3f8a5deb4', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('dde096b3-340f-c6be-5f29-98d3f8a5deb4'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('dde096b3-340f-c6be-5f29-98d3f8a5deb4');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('10b1600a-17e8-a1f8-abb7-9e903906f688'))
			{
				count = sourceDRGlobalKeyMap.get('10b1600a-17e8-a1f8-abb7-9e903906f688');
			}
			count++;
			sourceDRGlobalKeyMap.put('10b1600a-17e8-a1f8-abb7-9e903906f688', count);
		}
		destinationDRGlobalKeyMap.put('dde096b3-340f-c6be-5f29-98d3f8a5deb4', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		return destinationDRGlobalKeyMap;
	}

	public static Map<String, Map<String, Integer>> testDecomposition_8013h0000013Hb2AAE()
	{
		Map<String, Map<String, Integer>> destinationDRGlobalKeyMap = new Map<String, Map<String, Integer>>();
		Map<String, Integer> sourceDRGlobalKeyMap;
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('dde096b3-340f-c6be-5f29-98d3f8a5deb4'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('dde096b3-340f-c6be-5f29-98d3f8a5deb4');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('1ec1176a-d6e8-da6e-37d4-9c2351aae136'))
			{
				count = sourceDRGlobalKeyMap.get('1ec1176a-d6e8-da6e-37d4-9c2351aae136');
			}
			count++;
			sourceDRGlobalKeyMap.put('1ec1176a-d6e8-da6e-37d4-9c2351aae136', count);
		}
		destinationDRGlobalKeyMap.put('dde096b3-340f-c6be-5f29-98d3f8a5deb4', sourceDRGlobalKeyMap);
		return destinationDRGlobalKeyMap;
	}

	public static Map<String, Map<String, Integer>> testDecomposition_8013h0000011j3IAAQ()
	{
		Map<String, Map<String, Integer>> destinationDRGlobalKeyMap = new Map<String, Map<String, Integer>>();
		Map<String, Integer> sourceDRGlobalKeyMap;
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('d615f4da-1d30-07c0-e07b-e4ed6838a2d7'))
			{
				count = sourceDRGlobalKeyMap.get('d615f4da-1d30-07c0-e07b-e4ed6838a2d7');
			}
			count++;
			sourceDRGlobalKeyMap.put('d615f4da-1d30-07c0-e07b-e4ed6838a2d7', count);
		}
		destinationDRGlobalKeyMap.put('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('1f045892-de43-a24f-d349-4cf7f2d71834'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('1f045892-de43-a24f-d349-4cf7f2d71834');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('c8292463-f32b-74c5-65a1-41d7046b0f2d'))
			{
				count = sourceDRGlobalKeyMap.get('c8292463-f32b-74c5-65a1-41d7046b0f2d');
			}
			count++;
			sourceDRGlobalKeyMap.put('c8292463-f32b-74c5-65a1-41d7046b0f2d', count);
		}
		destinationDRGlobalKeyMap.put('1f045892-de43-a24f-d349-4cf7f2d71834', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('d615f4da-1d30-07c0-e07b-e4ed6838a2d7'))
			{
				count = sourceDRGlobalKeyMap.get('d615f4da-1d30-07c0-e07b-e4ed6838a2d7');
			}
			count++;
			sourceDRGlobalKeyMap.put('d615f4da-1d30-07c0-e07b-e4ed6838a2d7', count);
		}
		destinationDRGlobalKeyMap.put('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('1f045892-de43-a24f-d349-4cf7f2d71834'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('1f045892-de43-a24f-d349-4cf7f2d71834');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('c8292463-f32b-74c5-65a1-41d7046b0f2d'))
			{
				count = sourceDRGlobalKeyMap.get('c8292463-f32b-74c5-65a1-41d7046b0f2d');
			}
			count++;
			sourceDRGlobalKeyMap.put('c8292463-f32b-74c5-65a1-41d7046b0f2d', count);
		}
		destinationDRGlobalKeyMap.put('1f045892-de43-a24f-d349-4cf7f2d71834', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		return destinationDRGlobalKeyMap;
	}

	public static Map<String, Map<String, Integer>> testDecomposition_8013h0000011j3DAAQ()
	{
		Map<String, Map<String, Integer>> destinationDRGlobalKeyMap = new Map<String, Map<String, Integer>>();
		Map<String, Integer> sourceDRGlobalKeyMap;
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('d615f4da-1d30-07c0-e07b-e4ed6838a2d7'))
			{
				count = sourceDRGlobalKeyMap.get('d615f4da-1d30-07c0-e07b-e4ed6838a2d7');
			}
			count++;
			sourceDRGlobalKeyMap.put('d615f4da-1d30-07c0-e07b-e4ed6838a2d7', count);
		}
		destinationDRGlobalKeyMap.put('23bfc192-c0e4-c66b-bd15-f2a8d07b6b4f', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('1f045892-de43-a24f-d349-4cf7f2d71834'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('1f045892-de43-a24f-d349-4cf7f2d71834');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('c8292463-f32b-74c5-65a1-41d7046b0f2d'))
			{
				count = sourceDRGlobalKeyMap.get('c8292463-f32b-74c5-65a1-41d7046b0f2d');
			}
			count++;
			sourceDRGlobalKeyMap.put('c8292463-f32b-74c5-65a1-41d7046b0f2d', count);
		}
		destinationDRGlobalKeyMap.put('1f045892-de43-a24f-d349-4cf7f2d71834', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		return destinationDRGlobalKeyMap;
	}

	public static Map<String, Map<String, Integer>> testDecomposition_8013h0000011j3NAAQ()
	{
		Map<String, Map<String, Integer>> destinationDRGlobalKeyMap = new Map<String, Map<String, Integer>>();
		Map<String, Integer> sourceDRGlobalKeyMap;
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('413319ee-531a-cc0a-abe6-158550c49018'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('413319ee-531a-cc0a-abe6-158550c49018');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('8e88078e-d784-ee57-ec9a-87dbeb1a0de2'))
			{
				count = sourceDRGlobalKeyMap.get('8e88078e-d784-ee57-ec9a-87dbeb1a0de2');
			}
			count++;
			sourceDRGlobalKeyMap.put('8e88078e-d784-ee57-ec9a-87dbeb1a0de2', count);
		}
		destinationDRGlobalKeyMap.put('413319ee-531a-cc0a-abe6-158550c49018', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('413319ee-531a-cc0a-abe6-158550c49018'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('413319ee-531a-cc0a-abe6-158550c49018');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('413319ee-531a-cc0a-abe6-158550c49018', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('413319ee-531a-cc0a-abe6-158550c49018'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('413319ee-531a-cc0a-abe6-158550c49018');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('d2fc20a5-ddac-3f65-577a-35060d333959'))
			{
				count = sourceDRGlobalKeyMap.get('d2fc20a5-ddac-3f65-577a-35060d333959');
			}
			count++;
			sourceDRGlobalKeyMap.put('d2fc20a5-ddac-3f65-577a-35060d333959', count);
		}
		destinationDRGlobalKeyMap.put('413319ee-531a-cc0a-abe6-158550c49018', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('8e88078e-d784-ee57-ec9a-87dbeb1a0de2'))
			{
				count = sourceDRGlobalKeyMap.get('8e88078e-d784-ee57-ec9a-87dbeb1a0de2');
			}
			count++;
			sourceDRGlobalKeyMap.put('8e88078e-d784-ee57-ec9a-87dbeb1a0de2', count);
		}
		destinationDRGlobalKeyMap.put('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('d2fc20a5-ddac-3f65-577a-35060d333959'))
			{
				count = sourceDRGlobalKeyMap.get('d2fc20a5-ddac-3f65-577a-35060d333959');
			}
			count++;
			sourceDRGlobalKeyMap.put('d2fc20a5-ddac-3f65-577a-35060d333959', count);
		}
		destinationDRGlobalKeyMap.put('08f6a9ef-45e9-dc6a-6d85-e7a98e6b4801', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('859d58f9-2274-d717-b661-49d58cd6390e'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('859d58f9-2274-d717-b661-49d58cd6390e');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('859d58f9-2274-d717-b661-49d58cd6390e', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('c94b8510-c464-e9db-8a1f-ad615afea11b'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('c94b8510-c464-e9db-8a1f-ad615afea11b');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('c94b8510-c464-e9db-8a1f-ad615afea11b', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('a04eddd7-3f37-ffaf-2046-e6108b5861c5'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('a04eddd7-3f37-ffaf-2046-e6108b5861c5');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('a04eddd7-3f37-ffaf-2046-e6108b5861c5', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('beec356f-881a-78c4-f83b-610fe75db48f'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('beec356f-881a-78c4-f83b-610fe75db48f');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('beec356f-881a-78c4-f83b-610fe75db48f', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('96df3947-cec7-3bb2-61ec-dcbaadc02b34'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('96df3947-cec7-3bb2-61ec-dcbaadc02b34');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('96df3947-cec7-3bb2-61ec-dcbaadc02b34', sourceDRGlobalKeyMap);
		sourceDRGlobalKeyMap = new Map<String, Integer>();
		if (destinationDRGlobalKeyMap.containsKey('2f4d5c34-1eb6-e59e-914f-6c06d6a41d6a'))
		{
			sourceDRGlobalKeyMap = destinationDRGlobalKeyMap.get('2f4d5c34-1eb6-e59e-914f-6c06d6a41d6a');
			Integer count = 0;
			if (sourceDRGlobalKeyMap.containsKey('6b9b1b28-0690-cc72-f731-e7f13880fa35'))
			{
				count = sourceDRGlobalKeyMap.get('6b9b1b28-0690-cc72-f731-e7f13880fa35');
			}
			count++;
			sourceDRGlobalKeyMap.put('6b9b1b28-0690-cc72-f731-e7f13880fa35', count);
		}
		destinationDRGlobalKeyMap.put('2f4d5c34-1eb6-e59e-914f-6c06d6a41d6a', sourceDRGlobalKeyMap);
		return destinationDRGlobalKeyMap;
	}

}

