export interface Organization {
  Id: string;
  Name: string;
  NamespacePrefix: string;
}

export interface ApexClass {
  Name: string;
  NamespacePrefix: string;
}

export interface Product2 {
  Name: string;
  Id: string;
  GlobalKey: string;
  Scope: string;
}

export interface ProductChildItem {
  Id: string;
  Name: string;
  parentProductId: string;
  childProductId: string;
  min: number;
}

export interface DecompositionRelationship {
  Name: string;
  Id: string;
  SourceProductId: string;
  DestinationProductId: string;
  MappingsData: string;
  ConditionData: string;
}

export interface Attribute {
  Name: string;
  Code: string;
  Id: string;
  ActiveFlg: string;
  AttributeCategoryId: string;
  AttributeCategoryCode: string;
  AttributeCategoryName: string;
  DisplaySequence: string;
  Value: string;
  ValueType: string;
}

export interface AttributeCategory {
  Name: string;
  Id: string;
  DisplaySequence: string;
  Code: string;
}

export interface AttributeAssignment {
  Name: string;
  Id: string;
  ObjectId: string;
  AttributeId: string;
  AttributeCategoryId: string;
  Value: string;
}

export interface Order {
  Name: string;
  Id: string;
  SupplementalAction: string;
  Product2IdArray: string[];
}

export interface OrderItem {
  Id: string;
  OrderId: string;
  Product2Id: string;
  LineNumber: string;
  Action: string;
  SupplementalAction: string;
  OrderItemNumber: string;
}

export interface FulfilmentRequestLine {
  Name: string;
  Id: string;
  FulfilmentRequestId: string;
  Product2Id: string;
}

export interface FulfilmentRequest {
  Name: string;
  Id: string;
  OrderId: string;
}

export interface FulfilmentRequestLineDecompRelationship {
  Id: string;
  DestinationFulfilmentRequestLineId: string;
  IsConditionFailed: string;
  SourceFulfilmentRequestLineId: string;
  SourceOrderItemId: string;
}