import log4js =  require("log4js");
import path = require("path");

let logger = log4js.getLogger();
let isLoggerConfigured:boolean = false;

export class ComparePluginLogger
{
  private constructor()
  {  
  }

  public static getLogger(appender:string):any
  {
    if (!isLoggerConfigured)
    {
      isLoggerConfigured = true;
      log4js.configure({
        appenders: { DecompositionCompare: { type: "dateFile", maxLogSize: 10485760, filename: path.join(__dirname, "../../../log/compare-plugin.log") },
        default: { type: "dateFile", maxLogSize: 10485760, filename: path.join(__dirname, "log/compare-plugin.log") }},
        categories: { default: { appenders: [appender], level: "info"} }
      });
    }
    return new ComparePluginLogger();
  }

  public debug(message:string)
  {
    logger.debug(message);
  }

  public info(message:string)
  {
    logger.info(message);
  }

  public error(message:string)
  {
    logger.error(message);
  }
}