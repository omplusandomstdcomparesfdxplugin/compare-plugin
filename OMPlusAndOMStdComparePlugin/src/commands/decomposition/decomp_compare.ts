import { flags, SfdxCommand } from '@salesforce/command';
import { Connection, Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';

import hash = require('object-hash');
import fs = require('fs');
import path = require("path");
import rimraf = require("rimraf");
import jsforce = require('jsforce');

import apexUnitTest from '../../../plugin-resources/apex-unit-test';
import FRLDecompRelationshipClassGenerate from './FRLDecompRelationshipClassGenerate';
import * as iface_name from './sobject._iface';
import comparePluginLogger = require('./ComparePluginLogger');

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('OMPlusAndOMStdComparePlugin', 'org');

let logger = comparePluginLogger.ComparePluginLogger.getLogger("DecompositionCompare");

let fRLDecompRelationship:FRLDecompRelationshipClassGenerate;

const orderItemMap = new Map<string, Map<string, iface_name.OrderItem>>();
const orderMap = new Map<string, iface_name.Order>();
const uniqueOrderMap = new Map<string, string>();
const product2Map = new Map<string, iface_name.Product2>();
const decompositionRelationshipMap = new Map<string, Map<string, iface_name.DecompositionRelationship>>();
const productChildItemMap = new Map<string, Map<string, iface_name.ProductChildItem>>();
const fulfilmentRequestLineMap = new Map<string, Map<string, iface_name.FulfilmentRequestLine>>();
const fulfilmentRequestMap = new Map<string, Map<string, iface_name.FulfilmentRequest>>();
const attributeMap = new Map<string, iface_name.Attribute>();
const attributeCategoryMap = new Map<string, iface_name.AttributeCategory>();
const attributeAssignmentMap = new Map<string, Map<string, iface_name.AttributeAssignment>>();
const frlDecompRelationshipMap = new Map<string, Array<iface_name.FulfilmentRequestLineDecompRelationship>>();
let pToPFRLDecompRelationshipMap = new Map<String, Array<String>>();

let createdProductIdSet = new Set<string>();
let createdDRMap = new Map<string, iface_name.DecompositionRelationship>();
let createdPCISet = new Set<string>();

const outDirName:string = './out';
const classesDirName:string = '../OMPlusAndOMStdCompareTest/classes/';
const testClassName = 'OMStdAndPlusDecompositionCompareTest';
const testClassHelperName = 'DecompositionCompareTestHelper';
const testClassXmlFile = 'OMStdAndPlusDecompositionCompareTest.cls-meta.xml';
const helperClassXmlFile = 'DecompositionCompareTestHelper.cls-meta.xml';
const xmlFilesPath = '../OMPlusAndOMStdCompareTest/xmlFiles';
let testFileName:string;

const TextAttributeType = "Text";
const NumberAttributeType = "Number";
const CheckboxAttributeType = "Checkbox";
const DecimalAttributeType = "Decimal";
const DatetimeAttributeType = "Datetime";
const PicklistAttributeType = "Picklist";
const MultiPicklistAttributeType = "MultiPicklist";

const destinationOrgUrl = 'https://rashmi-telco-109-dev-ed.my.salesforce.com/';
const sessionId = '00D3h000007olJq\!AQkAQDbyYOd0Xb.lMjqXG_Q.Z6LQjt8S0RuoSW0QRiQosjRhpHyepKZRXZ5shJUihzs8Els_mKgztFSy44KXuFrX_vgN1Mt_';

export default class DecompositionCompare extends SfdxCommand {

  public static description = messages.getMessage('commandDescription');

  public static examples = [
  `$ ./bin/run decomposition:decomp_compare --name myname --targetusername myOrg@example.com
  My hub org id is: 00Dxx000000001234
  This org's namespace is :telco_109_2
  Size of result: 34, fileName: export_Product2.json
  Size of result: 38, fileName: export_PCI.json
  Size of result: 15, fileName: export_DR.json
  Size of result: 1, fileName: export_AttributeCategory.json
  Size of result: 0, fileName: export_AttributeAssignment.json
  Size of result: 34, fileName: ./out/export_Order.json
  Size of result: 948, fileName: ./out/export_OrderItems.json
  Size of uniqueOrderMap is 16`
  ];

  public static args = [{name: 'file'}];

  protected static flagsConfig = {
    // flag with a value (-n, --name=VALUE)
    name: flags.string({char: 'n', description: messages.getMessage('nameFlagDescription')}),
    force: flags.boolean({char: 'f', description: messages.getMessage('forceFlagDescription')})
  };

  // Comment this out if your command does not require an org username
  protected static requiresUsername = true;

  // Comment this out if your command does not support a hub org username
  protected static supportsDevhubUsername = true;

  // Set this to true if your command requires a project workspace; 'requiresProject' is false by default
  protected static requiresProject = false;

  public async run(): Promise<AnyJson> {
    this.queryAndExportJSON();

    // Return an object to be displayed with --json
    return { orgId: this.org.getOrgId()};
  }

  private async queryAndExportJSON() {
    // this.org is guaranteed because requiresUsername=true, as opposed to supportsUsername
    //const conn = this.org.getConnection();

    let conn: Connection;
    try{
      conn = new jsforce.Connection({
        serverUrl : destinationOrgUrl,
        sessionId : sessionId
      });
    }
    catch(err: any)
    {
      logger.error(`Error in connecting to serverUrl : ${destinationOrgUrl} \n ${err}`);
      throw new SfdxError(`Error in connecting to serverUrl : ${destinationOrgUrl} \n ${err}`);
    }


    logger.info(`Successfully connected to serverUrl : ${destinationOrgUrl} sessionId : ${sessionId}`);

    // Query the org's namespace
    const orgQuery = 'select Id, Name, NamespacePrefix from Organization';
    const orgResult = await conn.query<iface_name.Organization>(orgQuery);

    const apexClassQuery = 'select NamespacePrefix from ApexClass LIMIT 1';
    const apexClassResult = await conn.query<iface_name.ApexClass>(apexClassQuery);
    let namespacePrefix : string = apexClassResult.records[0].NamespacePrefix;

    this.ux.log(`\n\nThis org\'s namespace is : ${namespacePrefix} and id is ${orgResult.records[0].Id}\n\n`);
    logger.info(`This org\'s namespace is : ${namespacePrefix} and id is ${orgResult.records[0].Id}`);

    this.createAnOutDirectory(outDirName);
    let outDirPath = path.join(outDirName, orgResult.records[0].Id);
    this.deleteFileIfExists(outDirPath);
    let orgOutputDir:string = this.createAnOutDirectory(outDirPath);
    let orgClassesDir:string = this.createAnOutDirectory(classesDirName);
    testFileName = path.join(orgClassesDir, testClassName +'.cls');
    this.deleteFileIfExists(testFileName);
    let testClassXmlFilePath = path.join(orgClassesDir, testClassXmlFile);
    let helperClassXmlFilePath = path.join(orgClassesDir, helperClassXmlFile);
    this.deleteFileIfExists(testClassXmlFilePath);
    this.deleteFileIfExists(helperClassXmlFilePath);
    this.copyFile(path.join(xmlFilesPath, testClassXmlFile), testClassXmlFilePath);
    this.copyFile(path.join(xmlFilesPath, helperClassXmlFile), helperClassXmlFilePath);

    const product2Type = 'Product2';
    const pCIType = 'ProductChildItem__c';
    const dRType = 'DecompositionRelationship__c';
    const attributeType = 'Attribute__c';
    const attributeCategoryType = 'AttributeCategory__c';
    const attributeAssignmentType = 'AttributeAssignment__c';
    const orderType = 'Order';
    const orderItemType = 'OrderItem';
    const frlType = 'FulfilmentRequestLine__c';
    const frType = 'FulfilmentRequest__c';
    const frlDecompRelationshipType = 'FulfilmentRequestLineDecompRelationship__c';

    const product2Query = 'Select Name, Id, ' + namespacePrefix + '__GlobalKey__c, ' + namespacePrefix + '__Scope__c from ' + product2Type;
    const pCIQuery = 'Select Name, Id, ' + namespacePrefix + '__ParentProductId__c, ' + namespacePrefix + '__ChildProductId__c, '
        + namespacePrefix + '__MinimumChildItemQuantity__c from ' + namespacePrefix + '__' + pCIType;
    const dRQuery = 'Select Name, Id, ' + namespacePrefix + '__SourceProductId__c, ' + namespacePrefix + '__DestinationProductId__c, '
        + namespacePrefix + '__MappingsData__c, ' + namespacePrefix + '__ConditionData__c from ' + namespacePrefix + '__' + dRType;
    const attributeQuery = 'Select Name, Id, ' + namespacePrefix + '__Code__c, ' + namespacePrefix + '__ActiveFlg__c, '  + namespacePrefix + '__AttributeCategoryId__c, '
        + namespacePrefix + '__AttributeCategoryCode__c, ' + namespacePrefix + '__AttributeCategoryName__c, ' + namespacePrefix + '__DisplaySequence__c, '
        + namespacePrefix + '__Value__c, '  + namespacePrefix + '__ValueType__c from ' + namespacePrefix + '__' + attributeType;
    const attributeCategoryQuery = 'Select Name, Id, ' + namespacePrefix + '__DisplaySequence__c, ' + namespacePrefix + '__Code__c from '
        + namespacePrefix + '__' + attributeCategoryType;
    const attributeAssignmentQuery = 'Select Name, Id, ' + namespacePrefix + '__ObjectId__c, ' + namespacePrefix + '__AttributeId__c, '
        + namespacePrefix + '__AttributeCategoryId__c, ' + namespacePrefix + '__Value__c from ' + namespacePrefix + '__' + attributeAssignmentType;
    const orderQuery = 'Select Name, Id, ' + namespacePrefix + '__SupplementalAction__c from ' + orderType;
    const orderItemsQuery = 'Select Id, OrderId, Product2Id, OrderItemNumber, ' + namespacePrefix + '__LineNumber__c, ' + namespacePrefix + '__Action__c, ' + namespacePrefix
        + '__SupplementalAction__c from ' + orderItemType;
 
    // Query the org for sobjects

    logger.info("queryAndExportJSON: Query for order objects");

    let orderResult = await conn.query<iface_name.Order>(orderQuery);
    this.putToOrderMap(orderResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, orderResult, orderType, namespacePrefix);
    while(orderResult.nextRecordsUrl != undefined && orderResult.nextRecordsUrl != null)
    {
      orderResult = await conn.queryMore<iface_name.Order>(orderResult.nextRecordsUrl);
      this.putToOrderMap(orderResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, orderResult, orderType, namespacePrefix);
    }
    
    logger.info("queryAndExportJSON: Query for product2 objects");

    let product2Result = await conn.query<iface_name.Product2>(product2Query);
    this.putToProduct2Map(product2Result, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, product2Result, product2Type, namespacePrefix);
    while(product2Result.nextRecordsUrl != undefined && product2Result.nextRecordsUrl != null)
    {
      product2Result = await conn.queryMore<iface_name.Product2>(product2Result.nextRecordsUrl);
      this.putToProduct2Map(product2Result, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, product2Result, product2Type, namespacePrefix);
    }

    logger.info("queryAndExportJSON: Query for product child items objects");

    let pCIResult = await conn.query<iface_name.ProductChildItem>(pCIQuery);
    this.putToProductChildItemMap(pCIResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, pCIResult, pCIType, namespacePrefix);
    while(pCIResult.nextRecordsUrl != undefined && pCIResult.nextRecordsUrl != null)
    {
      pCIResult = await conn.queryMore<iface_name.ProductChildItem>(pCIResult.nextRecordsUrl);
      this.putToProductChildItemMap(pCIResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, pCIResult, pCIType, namespacePrefix);
    }

    logger.info("queryAndExportJSON: Query for decomposition result objects");

    let dRResult = await conn.query<iface_name.DecompositionRelationship>(dRQuery);
    this.putToDecompositionRelationshipMap(dRResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, dRResult, dRType, namespacePrefix);
    while(dRResult.nextRecordsUrl != undefined && dRResult.nextRecordsUrl != null)
    {
      dRResult = await conn.queryMore<iface_name.DecompositionRelationship>(dRResult.nextRecordsUrl);
      this.putToDecompositionRelationshipMap(dRResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, dRResult, dRType, namespacePrefix);
    }

    logger.info("queryAndExportJSON: Query for attribute objects");

    let attributeResult = await conn.query<iface_name.Attribute>(attributeQuery);
    this.putToAttributeMap(attributeResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, attributeResult, attributeType, namespacePrefix);
    while(attributeResult.hasOwnProperty('nextRecordsUrl'))
    {
      attributeResult = await conn.queryMore<iface_name.Attribute>(attributeResult.nextRecordsUrl);
      this.putToAttributeMap(attributeResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, attributeResult, attributeType, namespacePrefix);
    }

    logger.info("queryAndExportJSON: Query for attribute category objects");

    let attributeCategoryResult = await conn.query<iface_name.AttributeCategory>(attributeCategoryQuery);
    this.putToAttributeCategoryMap(attributeCategoryResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, attributeCategoryResult, attributeCategoryType, namespacePrefix);
    while(attributeCategoryResult.hasOwnProperty('nextRecordsUrl'))
    {
      attributeCategoryResult = await conn.queryMore<iface_name.AttributeCategory>(attributeCategoryResult.nextRecordsUrl);
      this.putToAttributeCategoryMap(attributeCategoryResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, attributeCategoryResult, attributeCategoryType, namespacePrefix);
    }

    logger.info("queryAndExportJSON: Query for attribute assignment objects");

    let attributeAssignmentResult = await conn.query<iface_name.AttributeAssignment>(attributeAssignmentQuery);
    this.putToAttributeAssignmentMap(attributeAssignmentResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, attributeAssignmentResult, attributeAssignmentType, namespacePrefix);
    while(attributeAssignmentResult.hasOwnProperty('nextRecordsUrl'))
    {
      attributeAssignmentResult = await conn.queryMore<iface_name.AttributeAssignment>(attributeAssignmentResult.nextRecordsUrl);
      this.putToAttributeAssignmentMap(attributeAssignmentResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, attributeAssignmentResult, attributeAssignmentType, namespacePrefix);
    }

    logger.info("queryAndExportJSON: Query for order item objects");

    let orderItemsResult = await conn.query<iface_name.OrderItem>(orderItemsQuery);
    this.putToOrderItemMap(orderItemsResult, namespacePrefix);
    this.processResultAndWriteToFile(orgOutputDir, orderItemsResult, orderItemType, namespacePrefix);
    while(orderItemsResult.hasOwnProperty('nextRecordsUrl'))
    {
      orderItemsResult = await conn.queryMore<iface_name.OrderItem>(orderItemsResult.nextRecordsUrl);
      this.putToOrderItemMap(orderItemsResult, namespacePrefix);
      this.processResultAndWriteToFile(orgOutputDir, orderItemsResult, orderItemType, namespacePrefix);
    }

    if (!product2Result.records || product2Result.records.length <= 0) {
      throw new SfdxError(messages.getMessage('noSObjectsFound', [product2Type, this.org.getOrgId()]));
    }

    if (!dRResult.records || dRResult.records.length <= 0) {
      throw new SfdxError(messages.getMessage('noSObjectsFound', [dRType, this.org.getOrgId()]));
    }

    if (!orderResult.records || orderResult.records.length <= 0) {
      throw new SfdxError(messages.getMessage('noSObjectsFound', [orderType, this.org.getOrgId()]));
    }

    fRLDecompRelationship = new FRLDecompRelationshipClassGenerate(testClassHelperName, orgClassesDir);

    this.findUniqueOrders();

    await this.queryFulfilmentRequestObjects(namespacePrefix, frType, conn, orgOutputDir);
    await this.queryFulfilmentRequestLineObjects(namespacePrefix, frlType, conn, orgOutputDir);
    await this.queryFulfilmentRequestLineDecompRelationshipObjects(namespacePrefix, frlDecompRelationshipType, conn, orgOutputDir);

    this.createApexUnitTest();
  }

  private async queryFulfilmentRequestObjects(namespacePrefix, frType, conn:Connection, outputDir){
    logger.info("queryFulfilmentRequestObjects: Query for fulfilment request object");
    let orderIdWhereClause = this.createWhereClause(namespacePrefix + `__OrderId__c`, uniqueOrderMap.values());

    for(let whereClause of orderIdWhereClause)
    {
      const fulfilmentRequestQuery = `Select Name, Id, ` + namespacePrefix + `__OrderId__c from ` + namespacePrefix + `__` + frType + whereClause;
      let frResult = await conn.query<iface_name.FulfilmentRequest>(fulfilmentRequestQuery);
      this.putToFulfilmentRequestMap(frResult, namespacePrefix);
      this.processResultAndWriteToFile(outputDir, frResult, frType, namespacePrefix);

      while(frResult.hasOwnProperty('nextRecordsUrl'))
      {
        frResult = await conn.queryMore<iface_name.FulfilmentRequest>(frResult.nextRecordsUrl);
        this.putToFulfilmentRequestMap(frResult, namespacePrefix);
        this.processResultAndWriteToFile(outputDir, frResult, frType, namespacePrefix);
      }
    }
  }

  private async queryFulfilmentRequestLineObjects(namespacePrefix, frlType, conn:Connection, outputDir){
    logger.info("queryFulfilmentRequestLineObjects: Query for fulfilment request line object");
    let frIdWhereClause = this.createWhereClause(namespacePrefix + '__FulfilmentRequestID__r.' + namespacePrefix + '__OrderId__c', uniqueOrderMap.values());
    
    for(let whereClause of frIdWhereClause)
    {
      const fulfilmentRequestLineQuery = 'Select Name, Id, ' + namespacePrefix + '__Product2Id__c, ' + namespacePrefix + '__FulfilmentRequestID__c from ' + namespacePrefix + '__'
                                          + frlType + whereClause;
      let frlResult = await conn.query<iface_name.FulfilmentRequestLine>(fulfilmentRequestLineQuery);
      this.putToFulfilmentRequestLineMap(frlResult, namespacePrefix);
      this.processResultAndWriteToFile(outputDir, frlResult, frlType, namespacePrefix);
  
      while(frlResult.hasOwnProperty('nextRecordsUrl'))
      {
        frlResult = await conn.queryMore<iface_name.FulfilmentRequestLine>(frlResult.nextRecordsUrl);
        this.putToFulfilmentRequestLineMap(frlResult, namespacePrefix);
        this.processResultAndWriteToFile(outputDir, frlResult, frlType, namespacePrefix);
      }
    }
  }

  private async queryFulfilmentRequestLineDecompRelationshipObjects(namespacePrefix, frlDecompRelationshipType, conn:Connection, outputDir){
    logger.info("queryFulfilmentRequestLineDecompRelationshipObjects: Query for fulfilment request line decomposititon relationship object");
    let frIdWhereClause = this.createWhereClause(namespacePrefix + '__DestinationFulfilmentRequestLineId__r.' + namespacePrefix + '__FulfilmentRequestId__r.' + namespacePrefix + '__OrderId__c', uniqueOrderMap.values());
    for(let whereClause of frIdWhereClause)
    {
      const frlDecompRelationshipQuery = 'Select Id, '  + namespacePrefix + '__SourceOrderItemId__c, ' + namespacePrefix + '__SourceFulfilmentRequestLineId__c, '
                                        + namespacePrefix + '__DestinationFulfilmentRequestLineId__c from ' + namespacePrefix + '__' + frlDecompRelationshipType + whereClause;
      let frlDecompRelationshipResult = await conn.query<iface_name.FulfilmentRequestLineDecompRelationship>(frlDecompRelationshipQuery);
      this.putToFulfilmentRequestLineDecompRelationshipMap(frlDecompRelationshipResult, namespacePrefix);
      this.processResultAndWriteToFile(outputDir, frlDecompRelationshipResult, frlDecompRelationshipType, namespacePrefix);

      while(frlDecompRelationshipResult.hasOwnProperty('nextRecordsUrl'))
      {
        frlDecompRelationshipResult = await conn.queryMore<iface_name.FulfilmentRequestLineDecompRelationship>(frlDecompRelationshipResult.nextRecordsUrl);
        this.putToFulfilmentRequestLineDecompRelationshipMap(frlDecompRelationshipResult, namespacePrefix);
        this.processResultAndWriteToFile(outputDir, frlDecompRelationshipResult, frlDecompRelationshipType, namespacePrefix);
      }
    }
  }

  private createAnOutDirectory(outputDir:string): string {
    // Create an output directory
    if(!fs.existsSync(outputDir)){
      fs.mkdirSync(outputDir);
    }
    return outputDir;
  }

  private deleteFileIfExists(path:string)
  {
    if(fs.existsSync(path)){
      if (fs.lstatSync(path).isDirectory()) {
        rimraf.sync(path);
      }
      else{
        fs.unlinkSync(path);
      }
    }
  }

  private copyFile(fromPath:string, toPath:string)
  {
    if(!fs.existsSync(toPath)){
      fs.copyFileSync(fromPath, toPath);
    }
  }

  private processResultAndWriteToFile(outputDir, result, sobjectType, namespacePrefix) {
    let fileNamePrefix = 'export_';

    let productFileName = outputDir + '/' + fileNamePrefix + sobjectType + '.json';
    this.writeSyncToFile(result, productFileName, namespacePrefix);
  }

  private writeSyncToFile(result, fileName, namespacePrefix) {
    let outputString = `Size of result: ${result.records.length}, fileName: ${fileName}`;
    logger.info(outputString);
    fs.appendFileSync(fileName, JSON.stringify(result, null, 2));
  }

  private createWhereClause(fieldname:string, fieldValues): Array<String>
  {
    let whereClauseArray = new Array<String>();
    let whereClause :string = '';
    let fieldValuesCount: number = 0;
    for(let fieldValue of fieldValues)
    {
      if(whereClause.length == 0)
      {
        whereClause = ` WHERE ${fieldname} = \'${fieldValue}\'`;
      }
      else
      {
        whereClause = whereClause + ` OR ${fieldname} = \'${fieldValue}\'`;
      }
      fieldValuesCount++;
      if(fieldValuesCount == 100)
      {
        whereClauseArray.push(whereClause);
        whereClause = '';
        fieldValuesCount = 0;
      }
    }

    if(fieldValuesCount < 100 && fieldValuesCount >0)
    {
      whereClauseArray.push(whereClause);
    }

    return whereClauseArray;
  }

  private putToOrderItemMap(orderItemsResult, namespacePrefix) {
    for(let orderItem of orderItemsResult.records){
      if (orderMap.has(orderItem['OrderId']))
      {
        let oi:iface_name.OrderItem = {
          Id: orderItem['Id'],
          Action : orderItem[namespacePrefix + '__Action__c'],
          SupplementalAction : orderItem[namespacePrefix + '__SupplementalAction__c'],
          LineNumber : orderItem[namespacePrefix + '__LineNumber__c'],
          Product2Id : orderItem['Product2Id'],
          OrderId : orderItem['OrderId'],
          OrderItemNumber : orderItem['OrderItemNumber']
        };
        let oiMap = new Map<string, iface_name.OrderItem>();

        if(orderItemMap.has(oi.OrderId))
        {
          oiMap = orderItemMap.get(oi.OrderId);
        }

        oiMap.set(oi.Id, oi);

        orderItemMap.set(oi.OrderId, oiMap);
        orderMap.get(orderItem['OrderId']).Product2IdArray.push(oi.Product2Id);
      }
    }
    logger.info(`putToOrderItemMap: orderItemMap size: ${orderItemMap.size}`);
  }

  private putToOrderMap(orderResult, namespacePrefix) {
    for(let order of orderResult.records) {
      let o:iface_name.Order = {
        Id: order['Id'],
        Name : order['Name'],
        SupplementalAction : order[namespacePrefix + '__SupplementalAction__c'],
        Product2IdArray : []
      };
      orderMap.set(order['Id'], o);
    }
    logger.info(`putToOrderMap: orderMap size: ${orderMap.size}`);
  }

  private putToProduct2Map(product2Result, namespacePrefix) {
    for(let product2 of product2Result.records) {
      let prd2:iface_name.Product2 = {
        Id: product2['Id'],
        Name: product2['Name'],
        GlobalKey: product2[namespacePrefix + '__GlobalKey__c'],
        Scope: product2[namespacePrefix + '__Scope__c']
      };
      product2Map.set(product2['Id'], prd2);
    }
    logger.info(`putToProduct2Ma: product2Map size: ${product2Map.size}`);
  }

  private putToProductChildItemMap(pCIResult, namespacePrefix)
  {
      for (let pCIRecord of pCIResult.records)
      {
        if(pCIRecord['Name'] != 'Root PCI')
        {
          let pCI:iface_name.ProductChildItem = {
            Id : pCIRecord['Id'],
            Name : pCIRecord['Name'],
            parentProductId : pCIRecord[namespacePrefix + '__ParentProductId__c'],
            childProductId : pCIRecord[namespacePrefix + '__ChildProductId__c'],
            min : pCIRecord[namespacePrefix + '__MinimumChildItemQuantity__c']
          };
          let pCIMap = new Map<string, iface_name.ProductChildItem>();
          if(productChildItemMap.has(pCI['parentProductId']))
          {
            pCIMap = productChildItemMap.get(pCI['parentProductId']);
          }
          pCIMap.set(pCI['Id'], pCI);
          productChildItemMap.set(pCI['parentProductId'], pCIMap);
        } 
      }
    logger.info(`putToProductChildItemMap: productChildItemMap size: ${productChildItemMap.size}`);
  }

  private putToDecompositionRelationshipMap(dRResult, namespacePrefix)
  {
    for(let decompositionRelationship of dRResult.records)
    {
      let dR:iface_name.DecompositionRelationship = {
        Id : decompositionRelationship['Id'],
        Name : decompositionRelationship['Name'],
        SourceProductId : decompositionRelationship[namespacePrefix + '__SourceProductId__c'],
        DestinationProductId : decompositionRelationship[namespacePrefix + '__DestinationProductId__c'],
        MappingsData : decompositionRelationship[namespacePrefix + '__MappingsData__c'],
        ConditionData : decompositionRelationship[namespacePrefix + '__ConditionData__c']
      };
      let dRMap = new Map<string, iface_name.DecompositionRelationship>();
      if(decompositionRelationshipMap.has(dR['SourceProductId']))
      {
        dRMap = decompositionRelationshipMap.get(dR['SourceProductId']) 
      }
      dRMap.set(decompositionRelationship['Id'], dR);
      decompositionRelationshipMap.set(dR['SourceProductId'], dRMap);
    }
    logger.info(`putToDecompositionRelationshipMap: decompositionRelationshipMap size: ${decompositionRelationshipMap.size}`);
  }

  private putToFulfilmentRequestLineMap(frlResult, namespacePrefix)
  {
    for(let frlRecord of frlResult.records)
    {
      let frl:iface_name.FulfilmentRequestLine = {
        Id : frlRecord['Id'],
        Name : frlRecord['Name'],
        FulfilmentRequestId : frlRecord[namespacePrefix + '__FulfilmentRequestID__c'],
        Product2Id : frlRecord[namespacePrefix + '__Product2Id__c']
      };

      let frlMap = new Map<string, iface_name.FulfilmentRequestLine>();
      if(fulfilmentRequestLineMap.has(frl['FulfilmentRequestId']))
      {
        frlMap = fulfilmentRequestLineMap.get(frl['FulfilmentRequestId']);
      }
      frlMap.set(frl.Id, frl);
      fulfilmentRequestLineMap.set(frl.FulfilmentRequestId, frlMap);
    }
    logger.info(`putToFulfilmentRequestLineMap: fulfilmentRequestLineMap size: ${fulfilmentRequestLineMap.size}`);
  }

  private putToFulfilmentRequestMap(frResult, namespacePrefix)
  {
    for(let frRecord of frResult.records)
    {
      let fr:iface_name.FulfilmentRequest = {
        Id : frRecord['Id'],
        Name : frRecord['Name'],
        OrderId : frRecord[namespacePrefix + '__OrderId__c']
      };

      let frMap = new Map<string, iface_name.FulfilmentRequest>();
      if(fulfilmentRequestMap.has(fr['OrderId']))
      {
        frMap = fulfilmentRequestMap.get(fr['OrderId']);
      }
      frMap.set(fr['Id'], fr);
      fulfilmentRequestMap.set(fr['OrderId'], frMap);
    }
    logger.info(`putToFulfilmentRequestMap: fulfilmentRequestMap size: ${fulfilmentRequestMap.size}`);
  }

  private putToFulfilmentRequestLineDecompRelationshipMap(frlDecompRelationshipResult, namespacePrefix)
  {
    for(let frlDecompRelationshipRecord of frlDecompRelationshipResult.records)
    {
      let frlDecompRelationship:iface_name.FulfilmentRequestLineDecompRelationship = {
        Id: frlDecompRelationshipRecord['Id'],
        IsConditionFailed: frlDecompRelationshipRecord[namespacePrefix + '__IsConditionFailed__c'],
        SourceFulfilmentRequestLineId: frlDecompRelationshipRecord[namespacePrefix + '__SourceFulfilmentRequestLineId__c'],
        SourceOrderItemId: frlDecompRelationshipRecord[namespacePrefix + '__SourceOrderItemId__c'],
        DestinationFulfilmentRequestLineId: frlDecompRelationshipRecord[namespacePrefix + '__DestinationFulfilmentRequestLineId__c']
      };

      let frlDecompRelationshipArray = new Array<iface_name.FulfilmentRequestLineDecompRelationship>();
      if(frlDecompRelationshipMap.has(frlDecompRelationship['DestinationFulfilmentRequestLineId']))
      {
        frlDecompRelationshipArray = frlDecompRelationshipMap.get(frlDecompRelationship['DestinationFulfilmentRequestLineId']);
      }
      frlDecompRelationshipArray.push(frlDecompRelationship);
      frlDecompRelationshipMap.set(frlDecompRelationship['DestinationFulfilmentRequestLineId'], frlDecompRelationshipArray);
    }
    logger.info(`putToFulfilmentRequestLineDecompRelationshipMap: frlDecompRelationshipMap size: ${frlDecompRelationshipMap.size}`);
  }

  private putToAttributeCategoryMap(attributeCategoryResult, namespacePrefix)
  {
    for(let attrCategoryRecord of attributeCategoryResult.records)
    {
      let attrCategory: iface_name.AttributeCategory = {
        Id : attrCategoryRecord['Id'],
        Name : attrCategoryRecord['Name'],
        Code : attrCategoryRecord[namespacePrefix + '__Code__c'],
        DisplaySequence : attrCategoryRecord[namespacePrefix + '__DisplaySequence__c']
      };
      attributeCategoryMap.set(attrCategory.Id, attrCategory);
    }
    logger.info(`putToAttributeCategoryMap: attributeCategoryMap size: ${attributeCategoryMap.size}`);
  }

  private putToAttributeMap(attributeResult, namespacePrefix)
  {
    for(let attrRecord of attributeResult.records)
    {
      let attr:iface_name.Attribute = {
        Id : attrRecord['Id'],
        Name : attrRecord['Name'],
        Code : attrRecord[namespacePrefix + '__Code__c'],
        ActiveFlg : attrRecord[namespacePrefix + '__ActiveFlg__c'],
        AttributeCategoryId: attrRecord[namespacePrefix + '__AttributeCategoryId__c'],
        AttributeCategoryCode : attrRecord[namespacePrefix + '__AttributeCategoryCode__c'],
        AttributeCategoryName : attrRecord[namespacePrefix + '__AttributeCategoryName__c'],
        DisplaySequence : attrRecord[namespacePrefix + '__DisplaySequence__c'],
        Value : attrRecord[namespacePrefix + '__Value__c'],
        ValueType : attrRecord[namespacePrefix + '__ValueType__c']
      };
      attributeMap.set(attr['Id'], attr);
    }
    logger.info(`putToAttributeMap: attributeMap size: ${attributeMap.size}`);
  }

  private putToAttributeAssignmentMap(attributeAssignmentResult, namespacePrefix)
  {
    for(let attrAssignmentRecord of attributeAssignmentResult.records)
    {
      let attrAssignment: iface_name.AttributeAssignment = {
        Id : attrAssignmentRecord['Id'],
        Name : attrAssignmentRecord['Name'],
        ObjectId: attrAssignmentRecord[namespacePrefix + '__ObjectId__c'],
        AttributeId: attrAssignmentRecord[namespacePrefix + '__AttributeId__c'],
        AttributeCategoryId: attrAssignmentRecord[namespacePrefix + '__AttributeCategoryId__c'],
        Value : attrAssignmentRecord[namespacePrefix + '__Value__c']
      };
      let attrAssignMap = new Map<string, iface_name.AttributeAssignment>();
      if(attributeAssignmentMap.has(attrAssignment[namespacePrefix + '__ObjectId__c']))
      {
        attrAssignMap = attributeAssignmentMap.get(attrAssignment[namespacePrefix + '__ObjectId__c']);
      }
      attrAssignMap.set(attrAssignment['Id'], attrAssignment);
      attributeAssignmentMap.set(attrAssignment[namespacePrefix + '__ObjectId__c'], attrAssignMap);
    }
    logger.info(`putToAttributeAssignmentMap: attributeAssignmentMap size: ${attributeAssignmentMap.size}`);
  }

  private findUniqueOrders() {
    for(let order of orderMap.values())
    {
      if(orderItemMap.has(order.Id))
      {
        let oiArray = new Array<Array<String>>();

        for(let oItem of orderItemMap.get(order.Id).values())
        {
          let ord = new Array<String>();
          ord.push(oItem["Product2Id"]);
          ord.push(oItem["Action"]);
          ord.push(oItem["SupplementalAction"]);
          oiArray.push(ord);
        }
        oiArray = oiArray.sort(this.sortOrderItemsbyProduct2Id);
        let oiArrayString = oiArray.toString();
        oiArrayString = oiArrayString + " "+ order.SupplementalAction;
        let hashValue:string = this.hashObject(oiArrayString);

        if(!uniqueOrderMap.has(hashValue))
        {
          uniqueOrderMap.set(hashValue, order.Id);
        }
      }
    }
    logger.info(`findUniqueOrders: Size of uniqueOrderMap is ${uniqueOrderMap.size} orderMap.size is ${orderMap.size}`);
  }

  private sortOrderItemsbyProduct2Id(oi1, oi2)
  {
    if(oi1.Product2Id > oi2.Product2Id)
    {
      return 1;
    }
    else if(oi1.Product2Id < oi2.Product2Id)
    {
      return -1;
    }
    return 0;
  }

  private hashObject(obj: string): string
  {
    return hash(obj);
  }

  private createApexUnitTest()
  {
    for(let child of apexUnitTest.children)
    {
      if(child.name == 'create test class')
      {
        this.startTestClass(testClassName);
      }
      else if(child.name == 'iterate through unique orders list')
      {
        this.iterateUniqueOrders(child);
      }
      else if(child.name == 'end class')
      {
        this.endTestClassOrMethod(testFileName, '');
      }
    }
  }
  
  private iterateUniqueOrders(testMethodTree)
  {
    for(let orderId of uniqueOrderMap.values())
    {
      logger.debug(`iterateUniqueOrders: orderId is ${orderId}`);
      for(let methodChildren of testMethodTree.children)
      {
        logger.debug(`iterateUniqueOrders: ${methodChildren.name}`);
        if(methodChildren.name == 'create test method')
        {
          this.startTestMethod(orderId, testFileName, '\t');
          this.createMethodContent(methodChildren, orderId);       
        }
        else if(methodChildren.name == 'end method')
        {
          this.endTestClassOrMethod(testFileName, '\t');
        }
      }
      createdProductIdSet = new Set<string>();
      createdDRMap = new Map<string, iface_name.DecompositionRelationship>();
      createdPCISet = new Set<string>();
    }

    fRLDecompRelationship.createDecompRelationshipHelperClass(uniqueOrderMap.values());
  }

  private createMethodContent(testMethodTree, orderId)
  {
    for(let methodChildren of testMethodTree.children)
    {
      logger.debug(`createMethodContent: ${methodChildren.name}`);
      if(methodChildren.name == 'declare attribute and test fixture')
      {
        this.declareTestFixtureAndJSONAttributeFixture(testFileName, '\t\t');
      }
      else if(methodChildren.name == 'iterate through orderItems')
      {
        this.createProductCataloge(orderId, methodChildren);
      }
      else if(methodChildren.name == 'order')
      {
        for(let orderChild of methodChildren.children)
        {
          logger.debug(`createMethodContent: ${orderChild.name}`);
          if(orderChild.name == 'create order')
          {
            this.createOrder(testFileName, '\t\t');
          }
          else if(orderChild.name == 'add order item')
          {
            for(let orderItem of orderItemMap.get(orderId).values())
            {
              if(product2Map.has(orderItem.Product2Id))
              {
                let prd2 = product2Map.get(orderItem.Product2Id);
                this.addOrderItem(testFileName, orderItem.LineNumber, prd2.Name, '\t\t');
              }
            }
          }
          else if(orderChild.name == 'end order')
          {
            this.endOrder(testFileName, '\t\t');
          }
          else if(orderChild.name == 'persist fixture')
          {
            this.persistFixture(testFileName, '\t\t');
          }
        }
      }
      else if(methodChildren.name == 'order decomposition')
      {
        this.decomposeOrder(testFileName, '\t\t');
      }
      else if(methodChildren.name == 'inventory items')
      {
        
      }
      else if(methodChildren.name == 'assert')
      {
        let fr = fulfilmentRequestMap.get(orderId);
        if(fr != null || fr != undefined)
        {
          this.assertFulfilmentRequestSize(testFileName, fr.size.toString(), '\t\t');
          this.addOIToFRLRelationshipComparison(testFileName, '\t\t', orderId);
        }
        logger.info(`createMethodContent: fr is ${fr}`);
      }
    }
  }

  private createProductCataloge(orderId:string, methodChildren)
  {
    logger.info(`createProductCataloge: orderId is ${orderId} orderItem size is ${orderItemMap.get(orderId).size}`);
    for(let orderItem of orderItemMap.get(orderId).values())
    {
      logger.debug(`createProductCataloge: orderId is ${orderId} orderItem is ${orderItem}`);
      let prd2 = product2Map.get(orderItem.Product2Id);
      for(let orderItemChildren of methodChildren.children)
      {
        this.parseCatalogeTree(orderItemChildren, prd2);
      }

      //Create Product-Child and Decomposition Relationship among destination products.
      for(let dr of createdDRMap.values())
      {
        if (productChildItemMap.has(dr.DestinationProductId))
        {
          this.addProductChildItem_DestinationProduct(dr.DestinationProductId);
        }
        if (decompositionRelationshipMap.has(dr.DestinationProductId))
        {
          this.addDestinationRelationship_DestinationProduct(dr.DestinationProductId);
        }
      }
    }
  }

  private parseCatalogeTree(orderItemTreeChildren, prd2:iface_name.Product2)
  {
    for(let catalogeChildren of orderItemTreeChildren.children)
    {
      logger.info(`parseCatalogeTree: catalogeChildren.name is ${catalogeChildren.name}}`);
      if(catalogeChildren.name == 'product' && prd2 != undefined)
      {
        if(!createdProductIdSet.has(prd2.Id))
        {
          this.createProduct(testFileName, prd2.Name, prd2.Scope, prd2.GlobalKey, '\t\t');
          createdProductIdSet.add(prd2.Id);
        }

        for(let productChildren of catalogeChildren.children)
        {
          logger.info(`parseCatalogeTree: productChildren.name is ${productChildren.name}}`);
          if(productChildren.name == 'pci')
          {
            if (productChildItemMap.has(prd2.Id))
            {
              this.addProductChildItem(prd2);
            }
          }
          else if(productChildren.name == 'dr')
          {
            if (decompositionRelationshipMap.has(prd2.Id))
            {
              this.addDestinationRelationship(prd2);
            }
          }
          else if(productChildren.name == 'attribute')
          {
            if(attributeAssignmentMap.has(prd2.Id))
            {
              this.addAttributeAndAttributeCategory(prd2.Id);
            }
          }
        }
      }
    }

    
  }

  private addProductChildItem(prd1:iface_name.Product2)
  {
    for(let pci of productChildItemMap.get(prd1.Id).values())
    {
      if(!createdPCISet.has(pci.Id))
      {
        if(!createdProductIdSet.has(pci.childProductId))
        {
          let prd2 = product2Map.get(pci.childProductId);
          this.createProduct(testFileName, prd2.Name, prd2.Scope, prd2.GlobalKey, '\t\t');
          createdProductIdSet.add(prd2.Id);
        }
        let childPrd2 = product2Map.get(pci.childProductId);
        let parentPrd2 = product2Map.get(pci.parentProductId);
        this.createProductChildItem(testFileName, parentPrd2.Name, childPrd2.Name, '\t\t');
        createdPCISet.add(pci.Id);
      }
    }
  }

  private addDestinationRelationship(prd1:iface_name.Product2)
  {
    for(let dr of decompositionRelationshipMap.get(prd1.Id).values())
    {
      if(!createdDRMap.has(dr.Id))
      {
        if(!createdProductIdSet.has(dr.DestinationProductId) && product2Map.has(dr.DestinationProductId))
        {
          let prd2 = product2Map.get(dr.DestinationProductId);
          this.createProduct(testFileName, prd2.Name, prd2.Scope, prd2.GlobalKey, '\t\t');
          createdProductIdSet.add(prd2.Id);
        }
        if(product2Map.has(dr.DestinationProductId) && product2Map.has(dr.SourceProductId))
        {
          let destPrd2 = product2Map.get(dr.DestinationProductId);
          let srcPrd2 = product2Map.get(dr.SourceProductId);
          this.createDecompositionRelationship(testFileName, srcPrd2.Name, destPrd2.Name, '\t\t');
          this.addMappingData(dr);
          this.addEndCall(testFileName, '\t\t');
          createdDRMap.set(dr.Id, dr);
        }
      }
    }
  }

  private addProductChildItem_DestinationProduct(prdId)
  {
    for(let pci of productChildItemMap.get(prdId).values())
    {
      if(createdProductIdSet.has(pci.childProductId) && !createdPCISet.has(pci.Id))
      {
        let childPrd2 = product2Map.get(pci.childProductId);
        let parentPrd2 = product2Map.get(pci.parentProductId);
        this.createProductChildItem(testFileName, parentPrd2.Name, childPrd2.Name, '\t\t');
        createdPCISet.add(pci.Id);
      }
    }
  }

  private addDestinationRelationship_DestinationProduct(prdId)
  {
    for(let dr of decompositionRelationshipMap.get(prdId).values())
    {
      if(!createdDRMap.has(dr.Id))
      {
        if(!createdProductIdSet.has(dr.DestinationProductId))
        {
          let prd2 = product2Map.get(dr.DestinationProductId);
          this.createProduct(testFileName, prd2.Name, prd2.Scope, prd2.GlobalKey, '\t\t');
          createdProductIdSet.add(prd2.Id);
        }
        let destPrd2 = product2Map.get(dr.DestinationProductId);
        let srcPrd2 = product2Map.get(dr.SourceProductId);
        this.createDecompositionRelationship(testFileName, srcPrd2.Name, destPrd2.Name, '\t\t');

        this.addMappingData(dr);
        this.addConditionDataInDecompositionRelationship(dr.ConditionData);

        this.addEndCall(testFileName, '\t\t');
        createdDRMap.set(dr.Id, dr);
      }
    }
  }

  private addMappingData(dr:iface_name.DecompositionRelationship)
  {
    if(dr.MappingsData != null && dr.MappingsData != undefined)
    {
      let mappingDataArrayObj = JSON.parse(dr.MappingsData);

      for(let mappingDataObj of mappingDataArrayObj)
      {
        if(mappingDataObj["mapping_type"] == "ad-verbatim")
        {
          if(mappingDataObj["source_type"] == "Attribute")
          {
            this.addAttributeAdVerbatimMappingInDecompositionRelationship(testFileName, mappingDataObj["source_attr_code"], mappingDataObj["source_attr_name"], mappingDataObj["destination_attr_code"], mappingDataObj["destination_attr_name"], '\t\t');
          }
          else if(mappingDataObj["source_type"] == "Field")
          {
            this.addFieldAdVerbatimMappingInDecompositionRelationship(testFileName, mappingDataObj["source_field_name"], mappingDataObj["destination_attr_code"], mappingDataObj["destination_attr_name"], '\t\t');
          }
        }
        else if(mappingDataObj["mapping_type"] == "static")
        {
          this.addStaticMappingInDecompositionRelationship(testFileName, mappingDataObj["value"], mappingDataObj["destination_attr_code"], mappingDataObj["destination_attr_name"], '\t\t');
        }
        else if(mappingDataObj["mapping_type"] == "list")
        {
          if(mappingDataObj["source_type"] == "Attribute")
          {
            this.addAttributeListMappingInDecompositionRelationship(testFileName, mappingDataObj["transform_mappings"], mappingDataObj["source_attr_code"], mappingDataObj["source_attr_name"], mappingDataObj["destination_attr_code"], mappingDataObj["destination_attr_name"], '\t\t');
          }
          else if(mappingDataObj["source_type"] == "Field")
          {
            this.addFieldListMappingInDecompositionRelationship(testFileName, mappingDataObj["transform_mappings"], mappingDataObj["source_field_name"], mappingDataObj["destination_attr_code"], mappingDataObj["destination_attr_name"], '\t\t');
          }
        }
      }
    }
  }

  private addConditionDataInDecompositionRelationship(conditionData)
  {
    if(conditionData != null && conditionData["type"] == "AND")
    {
      for(let singleCondition of conditionData["singleconditions"])
      {
        this.addSingleConditionInDecompositionRelationship('\t\t', singleCondition, testFileName);
      }
    }
  }

  private appendCodeLinesArrayTestClassFile(codeLines: String[], fileName: string)
  {
      for(let codeLine of codeLines)
      {
        this.appendCodeLineTestClassFile(codeLine, fileName);
      }
  }

  private appendCodeLineTestClassFile(codeLines: String, fileName: string)
  {
      fs.appendFileSync(fileName, codeLines);
  }

  private startTestClass(className:string)
  {
    this.appendCodeLineTestClassFile('@IsTest' + '\n' + 'public with sharing class ' + className + '\n' + '{' + '\n', testFileName);
  }

  private startTestMethod(suffix:string, fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + '@IsTest' + '\n' + tabs + 'public static void testDecomposition_' + suffix + '()' + '\n' + tabs + '{' + '\n', fileName);
  }

  private endTestClassOrMethod(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + '}' + '\n\n', fileName);
  }

  private declareTestFixtureAndJSONAttributeFixture(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + 'XOMTestJSONAttributeFactory attrHelper = new XOMTestJSONAttributeFactory();' + '\n' +
      tabs + 'XOMTestFixtureHelper fixture = new XOMTestFixtureHelper(attrHelper);' + '\n\n', fileName);
  }

  private addAttributeAndAttributeCategory(prdId:string)
  {
    for(let attrAssignment of attributeAssignmentMap.get(prdId).values())
    {
      let attrCatId = attrAssignment['AttributeCategoryId'];
      let attrId = attributeAssignmentMap.get(prdId)['AttributeId'];
      this.createAttributeCategoryAndAttribute(testFileName, '\t\t', attributeCategoryMap.get(attrCatId), attributeMap.get(attrId));
    }
  }

  private createAttributeCategoryAndAttribute(fileName:string, tabs:string, attrCategory:iface_name.AttributeCategory, attr:iface_name.Attribute)
  {
    let jsonAttrVarName = 'jsonAttr_' + attrCategory.Code;

    this.appendCodeLineTestClassFile(`XOMTestJSONAttributeFactory.JSONAttr ${jsonAttrVarName} = attrHelper.jsonAttr();`, fileName);
    this.appendCodeLineTestClassFile(tabs + `${jsonAttrVarName}.cat(\'${attrCategory.Code}\', \'${attrCategory.Name}\').end();` + `\n`, fileName);
    if(attr.ValueType == TextAttributeType)
    {
      this.createTextAttribute(fileName, tabs + '\t', attr);
    }
    else if (attr.ValueType == NumberAttributeType)
    {
      this.createNumberAttribute(fileName, tabs + '\t', attr);
    }
    else if (attr.ValueType == DecimalAttributeType)
    {
      this.createDecimalAttribute(fileName, tabs + '\t', attr);
    }
    else if (attr.ValueType == DatetimeAttributeType)
    {
      this.createDatetimeAttribute(fileName, tabs + '\t', attr);
    }
    else if (attr.ValueType == CheckboxAttributeType)
    {
      this.createCheckboxAttribute(fileName, tabs + '\t', attr);
    }
    else if (attr.ValueType == PicklistAttributeType)
    {
      this.createPicklistAttribute(fileName, tabs + '\t', attr);
    }
    else if (attr.ValueType == MultiPicklistAttributeType)
    {
      this.createMultiPicklistAttribute(fileName, tabs + '\t', attr);
    }
  }

  private createTextAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.textAttr(\'${attr.Code}\', \'${attr.Name}\', \'${attr.ValueType}\', \'text\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createNumberAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.intAttr(\'${attr.Code}\', \'${attr.Name}\', \'${attr.ValueType}\', \'text\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createDecimalAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.decimalAttr(\'${attr.Code}\', \'${attr.Name}\', \'${attr.ValueType}\', \'text\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createDatetimeAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.datetimeAttr(\'${attr.Code}\', \'${attr.Name}\', \'${attr.ValueType}\', \'text\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createCheckboxAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.checkboxAttr(\'${attr.Code}\', \'${attr.Name}\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createPicklistAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.picklistAttr(\'${attr.Code}\', \'${attr.Name}\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createMultiPicklistAttribute(fileName:string, tabs:string, attr:iface_name.Attribute)
  {
    this.appendCodeLineTestClassFile(tabs + `.multiPicklistAttr(\'${attr.Code}\', \'${attr.Name}\', \'text\').end();` + `\n`, fileName); 
    this.appendCodeLineTestClassFile(tabs + `.value(\'${attr.Value}\')` + `\n`
        + tabs + `.defValue(\'${attr.Value}\')` + `\n` + `.end();` + `\n`, fileName);
  }

  private createProduct(fileName:string, prodName: string, scope: string, globalKey: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `fixture.prod(\'${prodName}\').scope(\'${scope}\').globalKey('${globalKey}').end();` + `\n`, fileName);   
  }

  private createProductChildItem(fileName:string, srcProdName: string, childProdName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `fixture.pci(\'${srcProdName}\', \'${childProdName}\').end();` + `\n`, fileName);   
  }

  private createDecompositionRelationship(fileName:string, srcProdName: string, dstProdName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `fixture.dr(\'${srcProdName}\', \'${dstProdName}\')` + `\n`, fileName);   
  }

  private addFieldAdVerbatimMappingInDecompositionRelationship(fileName:string, srcField: string, destAttrCode: string, destAttrName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `.adVerbatimMapping(\'${destAttrCode}\', \'${destAttrName}\')` + `\n`
                                  + tabs + `.srcField(\'${srcField}\')` + `\n`
                                  + tabs + `.end()`, fileName);
  }

  private addAttributeAdVerbatimMappingInDecompositionRelationship(fileName:string, srcAttrCode: string, srcAttrName: string, destAttrCode: string, destAttrName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `.adVerbatimMapping(\'${destAttrCode}\', \'${destAttrName}\')` + `\n`
                                  + tabs + `.srcAttr(\'${srcAttrCode}\', \'${srcAttrName}\')` + `\n`
                                  + tabs + `.end()`, fileName);
  }

  private addStaticMappingInDecompositionRelationship(fileName:string, value: string, destAttrCode: string, destAttrName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `.staticMapping(\'${destAttrCode}\', \'${destAttrName}\')` + `\n`
                                  + tabs + `.value(\'${value}\')` + `\n`
                                  + tabs + `.end()`, fileName);
  }

  private addAttributeListMappingInDecompositionRelationship(fileName:string, transform_mappings, srcAttrCode: string, srcAttrName: string, destAttrCode: string, destAttrName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `.listMapping(\'${destAttrCode}\', \'${destAttrName}\')` + `\n`
                                  + tabs + `.srcAttr(\'${srcAttrCode}\', \'${srcAttrName}\')`, fileName);
    for(let mapping of transform_mappings)
    {
      this.appendCodeLineTestClassFile(tabs + `.mapping(\'${mapping["source_value"]}\', \'${mapping["destination_value"]}\')`, fileName);
    }
  }

  private addFieldListMappingInDecompositionRelationship(fileName:string, transform_mappings, srcField: string, destAttrCode: string, destAttrName: string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `.listMapping(\'${destAttrCode}\', \'${destAttrName}\')` + `\n`
                                  + tabs + `.srcField(\'${srcField}\')` + `\n`, fileName);
    for(let mapping of transform_mappings)
    {
      this.appendCodeLineTestClassFile(tabs + `.mapping(\'${mapping["source_value"]}\', \'${mapping["destination_value"]}\')`, fileName);
    }
  }

  private addSingleConditionInDecompositionRelationship(tabs, singleCondition, fileName)
  {
    if(singleCondition.get("left-side-type") == "field-OrderItem")
    {
      if(singleCondition.get("op") == "=" && singleCondition.get("left-side-datatype") == "string")
      {
        this.appendCodeLineTestClassFile(tabs + `.eqCondition(${singleCondition.get("left-side")}, ${singleCondition.get("right-side")}).end()` + `\n`, fileName); 
      }
      else if(singleCondition.get("op") == "startsWith" && singleCondition.get("left-side-datatype") == "string")
      {
        this.appendCodeLineTestClassFile(tabs + `.startsWithCondition(${singleCondition.get("left-side")}, ${singleCondition.get("right-side")}).end()` + `\n`, fileName); 
      } 
    }
    else if(singleCondition.get("left-side-type") == "field-FRLine")
    {

    }
    else if(singleCondition.get("left-side-type") == "attribute")
    {
      if(singleCondition.get("op") == "=" && singleCondition.get("left-side-datatype") == "Text")
      {
        this.appendCodeLineTestClassFile(tabs + `.attributeEqualCondition(${singleCondition.get("left-side")}, ${singleCondition.get("right-side")}).end()` + `\n`, fileName); 
      }
    }
  }

  private addEndCall(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + '.end();' + '\n', fileName);   
  }

  private createOrder(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile('\n' + tabs + 'fixture' + '\n'
        + tabs + '.account(\'Test\')' + '\n'
        + tabs + '.order(false)' + '\n', fileName);
  }

  private addOrderItem(fileName:string, lineNumber:string, orderItem:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `.oi(\'${lineNumber}\', \'${orderItem}\').end()` + `\n`, fileName);
  }

  private endOrder(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + '.end()' + '\n'
        + tabs + '.end();' + '\n\n', fileName);
  }

  private decomposeOrder(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + 'XOMTestFixtureHelper.DecompositionResults firstOrderResult = fixture.decomposeOrder();' + '\n'
        + tabs + 'List<XOMOrderDomainObject> frList = firstOrderResult.frObjs;' + '\n\n', fileName);
  }

  private assertFulfilmentRequestSize(fileName:string, frListSize:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + `System.assertEquals(${frListSize}, frList.size());` + `\n`, fileName);
  }

  private persistFixture(fileName:string, tabs:string)
  {
    this.appendCodeLineTestClassFile(tabs + 'fixture.persist();' + '\n', fileName);
  }

  private addOIToFRLRelationshipComparison(fileName:string, tabs:string, orderId:string)
  {
    let codeLines:String[] = new Array<String>();
    codeLines.push(`\t\tList<FulfilmentRequestLineDecompRelationship__c> frlDecompRelationshipsList = [SELECT  Id, SourceOrderItemId__c, SourceOrderItemId__r.Product2Id__c`
      + `,SourceOrderItemId__r.Product2Id__r.GlobalKey__c, SourceFulfilmentRequestLineId__c`
      + `,SourceFulfilmentRequestLineId__r.Product2Id__c`
      + `,SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c , DestinationFulfilmentRequestLineId__c`
      + `,DestinationFulfilmentRequestLineId__r.Product2Id__c`
      + `,DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c from FulfilmentRequestLineDecompRelationship__c where DestinationFulfilmentRequestLineId__r.FulfilmentRequestID__r.OrderId__c = '${orderId}'];`);

    codeLines.push("\n");
    codeLines.push(`\t\tMap<String, Map<String, Integer>> frlDecompRelationshipsMap = ${testClassHelperName}.testDecomposition_${orderId}();`);
    codeLines.push("\n\n");
    codeLines.push("\t\tfor(FulfilmentRequestLineDecompRelationship__c frlDecomRelationship : frlDecompRelationshipsList)");
    codeLines.push("\n");
    codeLines.push("\t\t{");
    codeLines.push("\n");
    codeLines.push("\t\t\tString dstGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.DestinationFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);");
    codeLines.push("\n");
    codeLines.push("\t\t\tSystem.assert(frlDecompRelationshipsMap.containsKey(dstGlobalKey));");
    codeLines.push("\n\n");
    codeLines.push("\t\t\tMap<String, Integer> srcMap = frlDecompRelationshipsMap.get(dstGlobalKey);");
    codeLines.push("\n");
    codeLines.push("\t\t\tString srcGlobalKey = null;");
    codeLines.push("\n\n");
    codeLines.push("\t\t\tif(frlDecomRelationship.SourceFulfilmentRequestLineId__r != null)");
    codeLines.push("\n");
    codeLines.push("\t\t\t{");
    codeLines.push("\n");
    codeLines.push("\t\t\t\tsrcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceFulfilmentRequestLineId__r.Product2Id__r.GlobalKey__c);");
    codeLines.push("\n");
    codeLines.push("\t\t\t\tSystem.assert(srcMap.containsKey(srcGlobalKey));");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t}");
    codeLines.push("\n");
    codeLines.push("\t\t\telse if(frlDecomRelationship.SourceOrderItemId__r != null)");
    codeLines.push("\n");
    codeLines.push("\t\t\t{");
    codeLines.push("\n");
    codeLines.push("\t\t\t\tsrcGlobalKey = XOMUtils.getSafeString(frlDecomRelationship.SourceOrderItemId__r.Product2Id__r.GlobalKey__c);");
    codeLines.push("\n");
    codeLines.push("\t\t\t\tSystem.assert(srcMap.containsKey(srcGlobalKey));");
    codeLines.push("\n");
    codeLines.push("\t\t\t}");
    codeLines.push("\n");
    codeLines.push("\t\t\tInteger count = srcMap.get(srcGlobalKey);");
    codeLines.push("\n");
    codeLines.push("\t\t\tcount--;");
    codeLines.push("\n\n");
    codeLines.push("\t\t\tif(count ==0)");
    codeLines.push("\n");
    codeLines.push("\t\t\t{");
    codeLines.push("\n");
    codeLines.push("\t\t\t\tsrcMap.remove(srcGlobalKey);");
    codeLines.push("\n\n");
    codeLines.push("\t\t\t\tif(srcMap.isEmpty())");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t{");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t\tfrlDecompRelationshipsMap.remove(dstGlobalKey);");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t}");
    codeLines.push("\n");
    codeLines.push("\t\t\t\telse");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t{");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t\tsrcMap.put(srcGlobalKey, count);");
    codeLines.push("\n");
    codeLines.push("\t\t\t\t}");
    codeLines.push("\n");
    codeLines.push("\t\t\t}");
    codeLines.push("\n");
    codeLines.push("\t\t}");
    codeLines.push("\n");

    this.appendCodeLinesArrayTestClassFile(codeLines, testFileName);
  }
}

//Export variables
export {fulfilmentRequestMap, frlDecompRelationshipMap, fulfilmentRequestLineMap, pToPFRLDecompRelationshipMap, product2Map, orderItemMap};
