import {GenerateApexClass} from './GenerateApexClass';

import {fulfilmentRequestMap, frlDecompRelationshipMap, fulfilmentRequestLineMap, pToPFRLDecompRelationshipMap, product2Map, orderItemMap} from './decomp_compare';

const destDRMapName:string = "destinationDRGlobalKeyMap";
const destDRMapType:string = "Map<String, Map<String, Integer>>";
const srcDRMapName:string = "sourceDRGlobalKeyMap";
const srcDRMapType:string = "Map<String, Integer>";

export default class FRLDecompRelationshipClassGenerate extends GenerateApexClass
{
    constructor(className:string, classesDirName:string)
    {
        super(destDRMapType, className, classesDirName);
    }

    public createDecompRelationshipHelperClass(orderIdArray:string[])
    {
        super.startClass();
        for(let orderId of orderIdArray)
        {
            this.createFRLDecompRelationshipClass(orderId);
        }
        super.endTestClassOrMethod("");
    }

    public createFRLDecompRelationshipClass(orderId:string)
    {
        if(!fulfilmentRequestMap.has(orderId))
        {
            return;
        }

        super.startMethod(orderId, "\t");
        this.createHashMap("\t\t", destDRMapName, destDRMapType);
        this.declareHashMap("\t\t", srcDRMapName, srcDRMapType);

        for(let fr of fulfilmentRequestMap.get(orderId).values())
        {
            for(let frl of fulfilmentRequestLineMap.get(fr.Id).values())
            {
                let frlId = frl.Id;
                if(frlDecompRelationshipMap.has(frlId))
                {
                    let srcFRLGlobalKeys = new Array<String>();
                    let destGlobalKey = this.getProduct2GlobalKeyForFRL(frlId, fr.Id);
                    let frlDecompRelationshipArray = frlDecompRelationshipMap.get(frlId);

                    for(let frlDecompRelationship of frlDecompRelationshipArray)
                    {
                        let srcGlobalKey = null;
                        this.emptyInitializeHashMap("\t\t", srcDRMapName, srcDRMapType);

                        if(frlDecompRelationship.SourceOrderItemId != undefined && frlDecompRelationship.SourceOrderItemId != null)
                        {
                            srcGlobalKey = this.getProduct2GlobalKeyForOrderItem(frlDecompRelationship.SourceOrderItemId, orderId);
                        }
                        else if(frlDecompRelationship.SourceFulfilmentRequestLineId != undefined && frlDecompRelationship.SourceFulfilmentRequestLineId != null)
                        {
                            srcGlobalKey = this.getProduct2GlobalKeyForFRL(frlDecompRelationship.SourceFulfilmentRequestLineId, fr.Id);
                        }
                        if(srcGlobalKey != null)
                        {
                            srcFRLGlobalKeys.push(srcGlobalKey);
                            this.addSrcGlobalKeyToHashMap("\t\t", destGlobalKey, srcGlobalKey, destDRMapName, srcDRMapName);
                        }
                    }
                    pToPFRLDecompRelationshipMap.set(destGlobalKey, srcFRLGlobalKeys);
                }
            }
        }
        this.addReturnValue("\t\t", destDRMapName);
        super.endTestClassOrMethod("\t");
    }

    private getProduct2GlobalKeyForFRL(frlId:string, frId:string):string
    {
        if(fulfilmentRequestLineMap.has(frId) && fulfilmentRequestLineMap.get(frId).has(frlId))
        {
            let product2Id = fulfilmentRequestLineMap.get(frId).get(frlId).Product2Id;
            let globalKey = product2Map.get(product2Id).GlobalKey;
            return globalKey;
        }

        return null;
    }

    private getProduct2GlobalKeyForOrderItem(orderItemId:string, orderId:string):string
    {
        let product2Id = orderItemMap.get(orderId).get(orderItemId).Product2Id;
        let globalKey = product2Map.get(product2Id).GlobalKey;

        return globalKey;
    }

    private createHashMap(tabs:string, mapName:string, type:string)
    {
        super.appendTestClassFile(tabs + `${type} ${mapName} = new ${type}();` + "\n");
    }

    private declareHashMap(tabs:string, mapName:string, type:string)
    {
        super.appendTestClassFile(tabs + `${type} ${mapName};` + "\n");
    }

    private emptyInitializeHashMap(tabs:string, mapName:string, type:string)
    {
        super.appendTestClassFile(tabs + `${mapName} = new ${type}();` + "\n");
    }

    private addSrcGlobalKeyToHashMap(tabs:string, destKey, srcKey, destMapName:string, srcMapName:string)
    {
        super.appendTestClassFile(tabs + `if (${destMapName}.containsKey('${destKey}'))` + `\n`);
        super.appendTestClassFile(tabs + '{' + `\n`);

        let oneMoreTab = tabs + '\t';
        super.appendTestClassFile(oneMoreTab + `${srcMapName} = ${destMapName}.get('${destKey}');` + `\n`);
        super.appendTestClassFile(oneMoreTab + 'Integer count = 0;' + `\n`);
        super.appendTestClassFile(oneMoreTab + `if (${srcMapName}.containsKey('${srcKey}'))` + `\n`);
        super.appendTestClassFile(oneMoreTab + '{' + `\n`);

        let twoMoreTab = oneMoreTab + '\t';
        super.appendTestClassFile(twoMoreTab + `count = ${srcMapName}.get('${srcKey}');` + `\n`);
        super.appendTestClassFile(oneMoreTab + '}' + '\n');
        super.appendTestClassFile(oneMoreTab  + `count++;` + `\n`);
        super.appendTestClassFile(oneMoreTab + `${srcMapName}.put('${srcKey}', count);` + '\n');
        super.appendTestClassFile(tabs + `}` + `\n`);
        super.appendTestClassFile(tabs + `${destMapName}.put('${destKey}', ${srcMapName});` + '\n');
    }

    private addReturnValue(tabs:string, variableName:string)
    {
        super.appendTestClassFile(tabs + `return ${variableName};` + `\n`);
    }
}