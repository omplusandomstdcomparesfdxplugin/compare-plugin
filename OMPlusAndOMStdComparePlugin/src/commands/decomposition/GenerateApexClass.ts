import fs = require('fs');

class GenerateApexClass
{
    className:string;
    fileName:string;
    returnType:string;

    constructor(returnType:string, className:string, classesDirName:string)
    {
        this.returnType = returnType;
        this.className = className;
        this.fileName = classesDirName + "/" + className + '.cls';
        this.deleteFileIfExists(this.fileName);
    }

    private deleteFileIfExists(path:string)
    {
        if(fs.existsSync(path)){
            fs.unlinkSync(path);
        }
    } 

    public appendTestClassFile(codeLines: string)
    {
        fs.appendFileSync(this.fileName, codeLines);
    }

    public annonatateClassOrMethod()
    {
        this.appendTestClassFile('@IsTest' + '\n');
    }

    public startClass()
    {
        this.appendTestClassFile('public with sharing class ' + this.className + '\n' + '{' + '\n');
    }

    public startTestClass()
    {
        this.annonatateClassOrMethod();
        this.startClass();
    }

    public startMethod(suffix:string, tabs:string)
    {
        this.appendTestClassFile(tabs + `public static ${this.returnType} testDecomposition_${suffix}()` + `\n` + `${tabs}{` + `\n`);
    }

    public startTestMethod(suffix:string, tabs:string)
    {
        this.annonatateClassOrMethod();
        this.startMethod(suffix, tabs);
    }

    public endTestClassOrMethod(tabs:string)
    {
        this.appendTestClassFile(tabs + '}' + '\n\n');
    }
}

export {GenerateApexClass};