OMPlusAndOMStdComparePlugin
===========================

This sfdx-cli plugin compares decomposition result from OMStd and OMPlus platform with each other.

[![Version](https://img.shields.io/npm/v/OMPlusAndOMStdComparePlugin.svg)](https://npmjs.org/package/OMPlusAndOMStdComparePlugin)
[![CircleCI](https://circleci.com/gh/Documents/OMPlusAndOMStdComparePlugin/tree/master.svg?style=shield)](https://circleci.com/gh/Documents/OMPlusAndOMStdComparePlugin/tree/master)
[![Appveyor CI](https://ci.appveyor.com/api/projects/status/github/Documents/OMPlusAndOMStdComparePlugin?branch=master&svg=true)](https://ci.appveyor.com/project/heroku/OMPlusAndOMStdComparePlugin/branch/master)
[![Codecov](https://codecov.io/gh/Documents/OMPlusAndOMStdComparePlugin/branch/master/graph/badge.svg)](https://codecov.io/gh/Documents/OMPlusAndOMStdComparePlugin)
[![Greenkeeper](https://badges.greenkeeper.io/Documents/OMPlusAndOMStdComparePlugin.svg)](https://greenkeeper.io/)
[![Known Vulnerabilities](https://snyk.io/test/github/Documents/OMPlusAndOMStdComparePlugin/badge.svg)](https://snyk.io/test/github/Documents/OMPlusAndOMStdComparePlugin)
[![Downloads/week](https://img.shields.io/npm/dw/OMPlusAndOMStdComparePlugin.svg)](https://npmjs.org/package/OMPlusAndOMStdComparePlugin)
[![License](https://img.shields.io/npm/l/OMPlusAndOMStdComparePlugin.svg)](https://github.com/Documents/OMPlusAndOMStdComparePlugin/blob/master/package.json)

# Install this plugin
To clone this plugin run this command: git clone git@bitbucket.org:omplusandomstdcomparesfdxplugin/compare-plugin.git
When cloning is complete a folder named 'compare-plugin' will be created.
Go inside compare-plugin/OMPlusAndOMStdComparePlugin folder and execute:

  $ npm install


# Commands to run this tool
To use this tool execute following commands in sequence:
  a) Authenticate on both source org and destionation org. A source org is one from which data is downloaded and unit tests are generated on basis of this data and a destination org is one where these unit tests are executed. Execute following commands:

    $ sfdx force:auth:web:login -a <source org login id>
    $ sfdx force:auth:web:login -a <destination org login id>

  b) Open compare-plugin/OMPlusAndOMStdComparePlugin/decomp_compare.ts file in an editor and search for 'const destinationOrgUrl' text. Paste session-id as fetched through tamper-monkey(https://vlocity.atlassian.net/wiki/spaces/XOM/pages/844595383/Salesforce+Logs+Downloader) and source org's url in compare-plugin/OMPlusAndOMStdComparePlugin/decomp_compare.ts file as value of session-id and destinationOrgUrl variables respectively. Make sure that values are single-quoted and exclamation mark ('!') in sessionId has back-slash before it as shown below:

  ```
  const destinationOrgUrl = 'https://xomthor109.my.salesforce.com/'
  ```
  
  ```
  const sessionId = '00D4W000000FJUf\\!AQIAQPUIMQTPCOLIdH6A.TaOqeRDx7jwRsH9d28nSX0ixJM_k9cjBViU7pdUIILS3zsjOkZxz951HIeOtJCuJUtvCMB_hVj_'
  ```

  c) Go inside compare-plugin/OMPlusAndOMStdComparePlugin folder and execute:

    $ ./bin/run decomposition:decomp_compare --name <user name> --targetusername <source org login id> -u <source org login id>
    
  This command will create a folder named as source-org's Id inside compare-plugin/OMPlusAndOMStdComparePlugin/out directory and all the sobjects information will be stored inside this folder. Moreover, it will also auto-generate a unit test class and save this class inside compare-plugin/OMPlusAndOMStdCompareTest/class path.

  d) To deploy auto-generated unit tests to destination org go inside compare-plugin/OMPlusAndOMStdCompareTest folder and execute:

    $ sfdx force:source:deploy -p "./classes/OMStdAndPlusDecompositionCompareTest.cls-meta.xml, ./classes/OMStdAndPlusDecompositionCompareTest.cls, ./classes/DecompositionCompareTestHelper.cls-meta.xml, ./classes/DecompositionCompareTestHelper.cls" -u <destination org login id>

  e) To excute the auto-generated unit tests on destination org execute following command

    $ sfdx force:apex:test:run --classnames "OMStdAndPlusDecompositionCompareTest" -u <destination org login id>

  f) Previous step's command will output another sfdx command to check unit test execution report. Copy this output command and paste it in command window to check the unit tests' result.
